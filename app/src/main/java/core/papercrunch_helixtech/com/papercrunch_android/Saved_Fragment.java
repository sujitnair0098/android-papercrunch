package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by helixtech-android on 12/11/16.
 */
public class Saved_Fragment extends Fragment {

    public Saved_Fragment() {
        // Required empty public constructor
    }
    ListView list;
    String[] subjects;
    String[] paper_codes;
    String[] exam_date;
    Integer[] imageId;
    String[] file_locs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.saved_fragment,
                container, false);

        //requesting permission on runtime to handle the android marshmallow version.
        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1) {
            String[] perms = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
            int permsRequestCode = 200;
            requestPermissions(perms, permsRequestCode);
        }


        Log.v(" test "," the current user id in saved fragment is "+PostValue.get_current_user_id());
        ArrayList<String> saved_paper_info = DBOps.get_saved_info(getContext(),PostValue.get_current_user_id());
        Log.v(" test "," the size is "+saved_paper_info.size());
        //the order in which the data is present in the arraylist is as follows.
        //0-subject name
        //1-subject code
        //2-exam data
        //3-saved location

        //assigning sizes to the arrays.
        subjects = new String[saved_paper_info.size()];
        paper_codes = new String[saved_paper_info.size()];
        exam_date = new String[saved_paper_info.size()];
        file_locs = new String[saved_paper_info.size()];

        imageId = new Integer[subjects.length];
        for(int i=0;i<subjects.length;i++)
        {
            imageId[i]=R.drawable.paper_pdf_image;
        }

        for(int i=0;i<saved_paper_info.size();i++)
        {
            String current_entry = saved_paper_info.get(i);
            //got the subject name
            subjects[i]=current_entry.substring(0,current_entry.indexOf("*"));
            current_entry=current_entry.substring(current_entry.indexOf("*")+1);

            //got the subject code
            paper_codes[i]=current_entry.substring(0,current_entry.indexOf("*"));
            current_entry=current_entry.substring(current_entry.indexOf("*")+1);

            //got the exam date
            exam_date[i]=current_entry.substring(0,current_entry.indexOf("*"));
            current_entry=current_entry.substring(current_entry.indexOf("*")+1);

            //got the file location
            file_locs[i]=current_entry;
        }

        CustomList_SavedPapers adapter = new
                CustomList_SavedPapers(getActivity(), subjects,paper_codes,exam_date, imageId);
        list=(ListView)view.findViewById(R.id.saved_paper_list);
        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id)
            {
                //open the pdf in a native call.
                File pdfFile = new File(file_locs[position]);
                Log.v(" test "," the item clicked is "+file_locs[position]);
                if (pdfFile.exists()) {

                    if (Build.VERSION.SDK_INT >= 21)
                    {
                        try
                        {
                            Intent open_pdf_renderer = new Intent(getActivity(),PDFRenderFragment.class);
                            open_pdf_renderer.putExtra("file",file_locs[position]);
                            getActivity().startActivity(open_pdf_renderer);
                        }
                        catch(Exception e)
                        {

                        }

                    }
                    else
                    {
                        Uri path = Uri.fromFile(pdfFile);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(path, "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActivity().startActivity(intent);
                    }

                }
            }
        });

//        pdf_frame_layout = (FrameLayout) view.findViewById(R.id.container);
//        pdf_frame_layout.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:

                boolean writeAccepted = grantResults[0]== PackageManager.PERMISSION_GRANTED;

                break;

        }

    }


    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }


    @Override
    public void onDestroy() {

        Log.v(" test "," on destroy called ");
        super.onDestroy();
    }

}