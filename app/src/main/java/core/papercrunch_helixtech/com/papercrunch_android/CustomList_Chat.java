package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by helixtech-android on 25/11/16.
 */
public class CustomList_Chat extends ArrayAdapter<String> {

    //Save_Paper save_paper;
    //    NotificationManager mNotifyManager;
//    NotificationCompat.Builder mBuilder;
//    int id=0;
//    String file_name;
//    String pdf_url = "https://www.papercrunch.in/examLibrary/CI3/2010/DEC/ci3-2010-dec-ci3.6.pdf";
    ProgressDialog progress;
    private final Activity context;
    private final String[] user_names;
    private final Integer[] imageId;
    private final String[] last_message;
    private final String[] last_time;
    public CustomList_Chat(Activity context,
                      String[] uName,String[] lMessage,String[] lTiming,Integer[] imageId)
    {
        super(context, R.layout.custom_chat_list, uName);
        this.context = context;
        this.user_names = uName;
        this.imageId = imageId;
        this.last_message = lMessage;
        this.last_time = lTiming;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.custom_chat_list, null, true);
        final TextView userName = (TextView) rowView.findViewById(R.id.user_name);
        TextView lastMessage = (TextView) rowView.findViewById(R.id.last_message);
        final TextView lastTiming = (TextView) rowView.findViewById(R.id.last_time);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.chat_image);
        final ImageView expand_image = (ImageView) rowView.findViewById(R.id.expand);
        final ImageView tick_image = (ImageView) rowView.findViewById(R.id.tick_mark);

        userName.setText(user_names[position]);
        lastMessage.setText(last_message[position]);
        lastTiming.setText(last_time[position]);
        imageView.setImageResource(imageId[position]);
        return rowView;
    }

}
