package core.papercrunch_helixtech.com.papercrunch_android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by helixtech-android on 9/12/16.
 */
public class InstanceServices extends FirebaseInstanceIdService
{
    private static final String TAG = "INSTANCE ID";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        String MY_PREFS_NAME = "mysharedpreferences";
        SharedPreferences.Editor editor = InstanceServices.this.getSharedPreferences(MY_PREFS_NAME, InstanceServices.this.MODE_PRIVATE).edit();
        editor.putString("devicetoken",refreshedToken);
        editor.commit();

        //PostValue.set_device_token(refreshedToken);

        AccountManager.updateToken(refreshedToken, InstanceServices.this);
        AccountManager.sendToken(this);
        // TODO: Implement this method to send any registration to your app's servers.
    }
}