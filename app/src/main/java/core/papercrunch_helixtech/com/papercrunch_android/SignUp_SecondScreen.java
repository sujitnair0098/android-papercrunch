package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SignUp_SecondScreen extends AppCompatActivity {

    RelativeLayout elective_sub_layout;
    int profile_pic_clicked=0;
    AutoCompleteTextView semester_dropdown;
    AutoCompleteTextView colg_dropdown,branch_dropdown;
    MultiAutoCompleteTextView backlog_subject_dropdown,backlog_semester_dropdown,elective_dropdown;
    Button semester_details_done;
    ProgressDialog progress;
    String email,password,first_name,last_name,pic_path;
    ImageView colg_name_image,branch_image,semester_image,elective_image,backlog_sem_image,backlog_sub_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        Intent myintent = getIntent();
        final String url = myintent.getStringExtra("URL");
        Log.v(" test "," url received is "+url);

        email = myintent.getStringExtra("EMAIL");
        password = myintent.getStringExtra("PASSWORD");
        first_name = myintent.getStringExtra("FIRSTNAME");
        last_name = myintent.getStringExtra("LASTNAME");
        profile_pic_clicked = Integer.parseInt(myintent.getStringExtra("PICCLICKED"));
        pic_path = myintent.getStringExtra("PICURL");

        setContentView(R.layout.activity_sign_up_second_screen);

        elective_sub_layout = (RelativeLayout) findViewById(R.id.elective_layout);
        elective_sub_layout.setVisibility(View.GONE);
        colg_dropdown = (AutoCompleteTextView) findViewById(R.id.college_name_spinner);
        colg_name_image = (ImageView) findViewById(R.id.college_name_button);
        branch_dropdown = (AutoCompleteTextView) findViewById(R.id.branch_name_spinner);
        branch_image = (ImageView) findViewById(R.id.branch_name_button);
        semester_dropdown = (AutoCompleteTextView) findViewById(R.id.semester_spinner);
        semester_image = (ImageView) findViewById(R.id.semester_name_button);
        elective_dropdown = (MultiAutoCompleteTextView) findViewById(R.id.elective_subjects_spinner);
        elective_image = (ImageView) findViewById(R.id.electives_name_button);
        backlog_semester_dropdown = (MultiAutoCompleteTextView) findViewById(R.id.backlog_semester_spinner);
        backlog_sem_image = (ImageView) findViewById(R.id.backlog_semester_button);
        backlog_subject_dropdown = (MultiAutoCompleteTextView) findViewById(R.id.backlog_semester_subject);
        backlog_sub_image = (ImageView) findViewById(R.id.backlog_subject_button);
        semester_details_done = (Button) findViewById(R.id.semester_details_done);

        colg_dropdown.setEnabled(false);
        branch_dropdown.setEnabled(false);
        semester_dropdown.setEnabled(false);
        elective_dropdown.setEnabled(false);
        backlog_semester_dropdown.setEnabled(false);
        backlog_subject_dropdown.setEnabled(false);

        //setting the showDropdowns for each respective image button
        colg_name_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                colg_dropdown.showDropDown();
            }
        });

        branch_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                branch_dropdown.showDropDown();
            }
        });

        semester_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                semester_dropdown.showDropDown();
            }
        });

        elective_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                elective_dropdown.showDropDown();
            }
        });

        backlog_sem_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backlog_semester_dropdown.showDropDown();
            }
        });

        backlog_sub_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backlog_subject_dropdown.showDropDown();
            }
        });


        //creating lists for the respective dropdowns.
        List<String> colleges = new ArrayList<String>();
        colleges.add("GEC");
        colleges.add("SRIEIT");
        colleges.add("PCCE");
        colleges.add("AGNEL POLYTECHNIC");
        colleges.add("NIT");
        colleges.add("DON BOSCO");

        List<String> branch = new ArrayList<String>();
        branch.add("Computers");
        branch.add("Mechanical");
        branch.add("Civil");
        branch.add("Mining");
        branch.add("Information Technology");
        branch.add("Electronics and Telecommunication");
        branch.add("Electrical and Electronics");

        List<String> semester = new ArrayList<String>();
        semester.add("I");
        semester.add("II");
        semester.add("III");
        semester.add("IV");
        semester.add("V");
        semester.add("VI");
        semester.add("VII");
        semester.add("VIII");

        List<String> elective = new ArrayList<String>();
        elective.add("Data Mining");
        elective.add("Web Technologies");
        elective.add("Mobile Computing");

        List<String> backlog_sem = new ArrayList<String>();
        backlog_sem.add("I");
        backlog_sem.add("II");
        backlog_sem.add("III");
        backlog_sem.add("IV");
        backlog_sem.add("V");
        backlog_sem.add("VI");
        backlog_sem.add("VII");
        backlog_sem.add("VIII");

        List<String> backlog_sub = new ArrayList<String>();
        backlog_sub.add("Data Mining");
        backlog_sub.add("Web Technologies");
        backlog_sub.add("Mobile Computing");

        //creating instances of array adapters for the respective dropdowns
        ArrayAdapter<String> colg_adapter = new ArrayAdapter<String>
                (this,android.R.layout.select_dialog_item,colleges);

        ArrayAdapter<String> branch_adapter = new ArrayAdapter<String>
                (this,android.R.layout.select_dialog_item,branch);

        ArrayAdapter<String> semester_adapter = new ArrayAdapter<String>
                (this,android.R.layout.select_dialog_item,semester);

        ArrayAdapter<String> elective_adapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1,elective);

        ArrayAdapter<String> backlog_sem_adapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1,backlog_sem);

        ArrayAdapter<String> backlog_sub_adapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1,backlog_sub);

        //setting the thesholds
        colg_dropdown.setThreshold(1);
        branch_dropdown.setThreshold(1);
        semester_dropdown.setThreshold(1);
        elective_dropdown.setThreshold(1);
        backlog_semester_dropdown.setThreshold(1);
        backlog_subject_dropdown.setThreshold(1);

        elective_dropdown.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        backlog_semester_dropdown.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        backlog_subject_dropdown.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        //setting the adapters to the autocomplete textviews
        colg_dropdown.setAdapter(colg_adapter);
        branch_dropdown.setAdapter(branch_adapter);
        semester_dropdown.setAdapter(semester_adapter);
        elective_dropdown.setAdapter(elective_adapter);
        backlog_semester_dropdown.setAdapter(backlog_sem_adapter);
        backlog_subject_dropdown.setAdapter(backlog_sub_adapter);


        //setting on dropdown item selected listeners for the autocomplete text views.
        colg_dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
               colg_dropdown.setGravity(Gravity.CENTER|Gravity.LEFT);
            }
        });

        branch_dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                branch_dropdown.setGravity(Gravity.CENTER|Gravity.LEFT);
            }
        });

        semester_dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                semester_dropdown.setGravity(Gravity.CENTER|Gravity.LEFT);
                if(!semester_dropdown.getText().toString().equals("VII") && !semester_dropdown.getText().toString().equals("VIII"))
                {
                    elective_sub_layout.setVisibility(View.GONE);
                }
                else
                {
                    elective_sub_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        elective_dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                elective_dropdown.setGravity(Gravity.CENTER|Gravity.LEFT);
            }
        });

        backlog_semester_dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                backlog_semester_dropdown.setGravity(Gravity.CENTER|Gravity.LEFT);
            }
        });

        backlog_subject_dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                backlog_subject_dropdown.setGravity(Gravity.CENTER|Gravity.LEFT);
            }
        });


        semester_details_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                progress = ProgressDialog.show(SignUp_SecondScreen.this, "Please Wait",
                        "", true);
                web_service_call(url);
            }
        });

    }

    private void web_service_call(String url)
    {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("Response", s);
                try {
                    Log.v(" test "," into try ");
                    JSONObject object = new JSONObject(s);
                    Log.v(" test "," response is "+object.getInt("response"));
                    if (object.getInt("response") == 1)
                    {
                        Log.v(" test "," sign up successful");


                        if(profile_pic_clicked != 1)
                        {
                            String MY_PREFS_NAME = "mysharedpreferences";
                            SharedPreferences.Editor editor = SignUp_SecondScreen.this.getSharedPreferences(MY_PREFS_NAME, SignUp_SecondScreen.this.MODE_PRIVATE).edit();
                            editor.putString("profile_pic_url", null);
                            editor.commit();

                            pic_path="";
                        }

                        //get the user id and save it in database.
                        String uID = ""+object.getInt("userid");
                        DBOps.insert_user_id(SignUp_SecondScreen.this,uID);

                        //insert the data in the academic details table.

                        Log.v(" test "," inserting academic details ");
                        Log.v(" test "," college "+colg_dropdown.getText().toString());
                        Log.v(" test "," branch "+branch_dropdown.getText().toString());
                        Log.v(" test "," sem "+semester_dropdown.getText().toString());
                        Log.v(" test "," elective is "+elective_dropdown.getText().toString());
                        Log.v(" test "," backlog is "+backlog_subject_dropdown.getText().toString());

                        DBOps.insert_academic_details(SignUp_SecondScreen.this,
                                uID,
                                colg_dropdown.getText().toString(),
                                branch_dropdown.getText().toString(),
                                semester_dropdown.getText().toString(),
                                elective_dropdown.getText().toString(),
                                backlog_subject_dropdown.getText().toString());


//                        //inserting the profile details
//                        DBOps.insert_profile_details(SignUp_SecondScreen.this,uID,first_name,last_name,email,password);

                        Toast.makeText(SignUp_SecondScreen.this,"Successful",Toast.LENGTH_SHORT).show();
                        store_data(uID);

                        Log.v(" test "," user id is "+uID);

                        PostValue.set_current_user_id(uID);
                       // inserting the profile path
                        DBOps.insert_profile_pic_path(SignUp_SecondScreen.this,pic_path,uID);

                        finish();
                        Intent open_main_activity = new Intent(SignUp_SecondScreen.this,MainActivity.class);
                        SignUp_SecondScreen.this.startActivity(open_main_activity);

                        try {

                            String database_name = DBHelper.DATABASE_NAME;
                            File sd = Environment.getExternalStorageDirectory();
                            File data = Environment.getDataDirectory();

                            if (sd.canWrite()) {
                                String currentDBPath = "//data//" + SignUp_SecondScreen.this.getPackageName() + "//databases//"
                                        + database_name + "";
                                String backupDBPath = "backupname.db";
                                File currentDB = new File(data, currentDBPath);
                                File backupDB = new File(sd, backupDBPath);

                                if (currentDB.exists()) {
                                    FileChannel src = new FileInputStream(currentDB).getChannel();
                                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                                    dst.transferFrom(src, 0, src.size());
                                    src.close();
                                    dst.close();
                                }
                            }
                        } catch (Exception e) {

                        }
                    }
                    else
                    {
                        Toast.makeText(SignUp_SecondScreen.this,"Failed",Toast.LENGTH_SHORT).show();

                        if(profile_pic_clicked != 1) {
                            String MY_PREFS_NAME = "mysharedpreferences";
                            SharedPreferences.Editor editor = SignUp_SecondScreen.this.getSharedPreferences(MY_PREFS_NAME, SignUp_SecondScreen.this.MODE_PRIVATE).edit();
                            editor.putString("profile_pic_url", null);
                            editor.commit();
                        }
                    }

                    progress.cancel();
                    //storeData(object, account);
                } catch (JSONException e)
                {
                    Log.v(" test "," into catch block ");
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)
            {
                Log.v(" test "," no internet connection ");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
//                data.put("designer_id", String.valueOf(SyncStateContract.Constants.designer_id));
//                return data;

                return null;

            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }


    public void store_data(String user_id)
    {
        String MY_PREFS_NAME="mysharedpreferences";
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("username",email);
        editor.putString("password",password);
        editor.putString("userid",user_id);
        editor.putString("firstname",first_name);
        editor.putString("lastname",last_name);
        editor.commit();

        //also store the data in the database.
        DBOps.insert_profile_details(SignUp_SecondScreen.this,user_id,first_name,last_name,email,password);

    }
}
