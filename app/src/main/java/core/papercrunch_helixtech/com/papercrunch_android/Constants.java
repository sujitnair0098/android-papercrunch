package core.papercrunch_helixtech.com.papercrunch_android;

/**
 * Created by admin on 02-06-2016.
 */
public class Constants {
    public static final int designer_id=52;//48
    public static int cartNumbers=0;

    public static final String designer_name= "Designer Name" ;
//    public static final String PUSH_NOTIFICATION = "pushnotification";
//    public static final int NOTIFICATION_ID = 235;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}
