package core.papercrunch_helixtech.com.papercrunch_android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Splashscreen extends AppCompatActivity {

    int i=0;
    int response;
    ArrayList<String> unsorted_list = new ArrayList<String>();
    ArrayList<String> values = new ArrayList<String>();
    ArrayList<String> user_ids = new ArrayList<String>();
    String[] unsorted_array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

//        String deviceId = Settings.Secure.getString(Splashscreen.this.getContentResolver(),
//                Settings.Secure.ANDROID_ID);
//
//        Log.v(" test "," the device token is "+deviceId);
//
//        String token = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");


        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
//        if (checkPlayServices()) {
//            gcm = GoogleCloudMessaging.getInstance(this);
//            regid = getRegistrationId(context);
//
//            if (regid.isEmpty()) {
//                registerInBackground();
//            }
//        } else {
//            Log.i(TAG, "No valid Google Play Services APK found.");
//        }


        setContentView(R.layout.activity_splashscreen);
       // new ProgressBack().execute();

        String MY_PREFS_NAME="mysharedpreferences";
        SharedPreferences prefs = Splashscreen.this.getSharedPreferences(MY_PREFS_NAME,Splashscreen.this.MODE_PRIVATE);
        String user_id = prefs.getString("userid",null);
        PostValue.set_current_user_id(user_id);
        Log.v(" test "," user id is "+user_id);
        String url = "http://httest.in/papercrunchws/RUC_GetLIstOfUsers/" +
                "user_id/"+user_id+"/" +
                "pageno/0";
        Log.v(" test "," url called is "+url);
        web_service_call(url);
    }


    private void web_service_call(String url)
    {
        i=Integer.parseInt(url.substring(url.lastIndexOf("/")+1));
        final StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("Response", s);
                try
                {
                    JSONObject object = new JSONObject(s);
                    if (object.getInt("response") == 1)
                    {
                        Log.v(" test "," response 1 ");
                        JSONArray jar = object.getJSONArray("data");
                        for (int i=0;i<jar.length();i++)
                        {
                            JSONObject rec = jar.getJSONObject(i);

                            if(!rec.getString("user_id").equals(PostValue.get_current_user_id()))
                            {
                                unsorted_list.add(rec.getString("firstName") + " " + rec.getString("lastName")+"*"+rec.getString("user_id"));
//                                values.add(rec.getString("firstName") + " " + rec.getString("lastName"));
//                                user_ids.add(rec.getString("user_id"));
                            }
                        }


                        String new_url = "http://httest.in/papercrunchws/RUC_GetLIstOfUsers/" +
                                "user_id/"+PostValue.get_current_user_id()+"/" +
                                "pageno/"+""+(i+1);
                        Log.v(" test "," new url called is "+new_url);
                        web_service_call(new_url);
                    }
                    else
                    {
                        //first sort the unsorted arraylist.
                        //Then split each entry to get the name and the user id.
                        unsorted_array = new String[unsorted_list.size()];
                        for(int i=0;i<unsorted_list.size();i++)
                        {
                            unsorted_array[i]=unsorted_list.get(i);
                        }

                        Arrays.sort(unsorted_array);

                        for(int i=0;i<unsorted_array.length;i++)
                        {
                            values.add(unsorted_array[i].substring(0,unsorted_array[i].lastIndexOf("*")));
                            user_ids.add(unsorted_array[i].substring(unsorted_array[i].lastIndexOf("*")+1));
                        }
                        PostValue.set_user_list(values);
                        PostValue.set_user_id_list(user_ids);
                        finish();
                        Intent i = new Intent(Splashscreen.this, WelcomeActivity.class);
                        startActivity(i);
                    }

                }
                catch (JSONException e)
                {
                    Log.v(" test "," into catch block ");
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)
            {
                Log.v(" test "," no internet connection ");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;

            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(request);

    }
}
