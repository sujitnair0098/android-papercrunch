package core.papercrunch_helixtech.com.papercrunch_android;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBOps {

	// function used to insert the notations in the notation table
	public static void insert_paper_info_on_save(Context context,String user_id,String sub,String code,String exam_date,String location)
	{

		DBHelper dbHelper = new DBHelper(context); // call the constructor
													// //this will create the db
													// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		// inserting into the PARENT table
		ContentValues paper_save_info = new ContentValues();
		paper_save_info.put(DBContract.Content.paper_subject_name,sub);
		paper_save_info.put(DBContract.Content.paper_exam_date,exam_date);
		paper_save_info.put(DBContract.Content.paper_subject_code,code);
		paper_save_info.put(DBContract.Content.paper_save_location,location);
		paper_save_info.put(DBContract.Content.user_id,user_id);
		db.insert(DBContract.Content.PAPER_TABLE, null, paper_save_info);
		//db.update(DBContract.Content.PAPER_TABLE, paper_save_info, null, null);
	}

	//function to retreive the saved paper info
	public static ArrayList<String> get_saved_info(Context context,String user_id) {

		ArrayList<String> saved_info_list = new ArrayList<String>();
		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Cursor cu = db
				.rawQuery("SELECT " + DBContract.Content.paper_subject_name + ","+
						 DBContract.Content.paper_subject_code +","
						+ DBContract.Content.paper_exam_date +","
						+ DBContract.Content.paper_save_location
						+" FROM " + DBContract.Content.PAPER_TABLE
						+ " where " + DBContract.Content.user_id + "='" + user_id + "'", null);

		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
			if (cu.getString(0) != null && cu.getString(0) != "") {
				saved_info_list.add(cu.getString(0)+"*"+cu.getString(1)+"*"+cu.getString(2)+"*"+cu.getString(3));
			}
		}
		cu.close();
		return saved_info_list;
	}



	// function used to insert the notations in the notation table
	public static void insert_academic_details(Context context,String user_id,String colg,String branch,String sem,String elective,String backlogs)
	{

		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		// inserting into the ACADEMIC table
		ContentValues academic_save_info = new ContentValues();
		academic_save_info.put(DBContract.Content.college_name,colg);
		academic_save_info.put(DBContract.Content.college_branch,branch);
		academic_save_info.put(DBContract.Content.current_semester,sem);
		academic_save_info.put(DBContract.Content.elective_subjects,elective);
		academic_save_info.put(DBContract.Content.backlog_subjects,backlogs);
		//db.insert(DBContract.Content.ACADEMIC_DETAILS, null, academic_save_info);
		db.update(DBContract.Content.ACADEMIC_DETAILS, academic_save_info,
				DBContract.Content.user_id + "='" + user_id + "'", null);
	}


	//function to retrieve the saved academic details.
	public static ArrayList<String> get_saved_academic_details(Context context,String user_id) {

		ArrayList<String> saved_info_list = new ArrayList<String>();
		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Cursor cu = db
				.rawQuery("SELECT " + DBContract.Content.college_name + ","+
						DBContract.Content.college_branch +","
						+ DBContract.Content.current_semester +","
						+ DBContract.Content.elective_subjects +","
						+ DBContract.Content.backlog_subjects +" FROM " + DBContract.Content.ACADEMIC_DETAILS
						+ " where " + DBContract.Content.user_id + "='" + user_id + "'", null);

		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
			if (cu.getString(0) != null && cu.getString(0) != "") {
				saved_info_list.add(cu.getString(0)+"*"+cu.getString(1)+"*"+cu.getString(2)+"*"+cu.getString(3)+"*"+cu.getString(4));
			}
		}
		cu.close();
		return saved_info_list;
	}

	//function to insert profile details in database
	public static void insert_profile_details(Context context,String user_id,String fName,String lName,String eMail,String pWD)
	{

		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Log.v(" test "," entered here ");
		// inserting into the PROFILE table
		ContentValues profile_save_info = new ContentValues();
		profile_save_info.put(DBContract.Content.first_name,fName);
		profile_save_info.put(DBContract.Content.last_name,lName);
		profile_save_info.put(DBContract.Content.email,eMail);
		profile_save_info.put(DBContract.Content.password,pWD);
		db.update(DBContract.Content.PROFILE_DETAILS, profile_save_info,
				DBContract.Content.user_id + "='" + user_id + "'", null);
	}


	//function to retrieve the saved profile details.
	public static ArrayList<String> get_saved_profile_details(Context context,String user_id) {

		ArrayList<String> saved_info_list = new ArrayList<String>();
		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Cursor cu = db
				.rawQuery("SELECT " + DBContract.Content.first_name + ","+
						DBContract.Content.last_name +","
						+ DBContract.Content.email +","
						+ DBContract.Content.password+" FROM " + DBContract.Content.PROFILE_DETAILS
						+ " where " + DBContract.Content.user_id + "='" + user_id + "'", null);

		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
			if (cu.getString(0) != null && cu.getString(0) != "") {
				saved_info_list.add(cu.getString(0)+"*"+cu.getString(1)+"*"+cu.getString(2)+"*"+cu.getString(3));
			}
		}
		cu.close();
		return saved_info_list;
	}

	//function to insert user id in database
	public static void insert_user_id(Context context,String user_id)
	{

		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Log.v(" test "," entered here ");
		// inserting into the PROFILE table
		ContentValues save_user_id = new ContentValues();
		save_user_id.put(DBContract.Content.user_id,user_id);
		db.insert(DBContract.Content.ACADEMIC_DETAILS, null, save_user_id);
		db.insert(DBContract.Content.PROFILE_DETAILS, null, save_user_id);
	}

	//function to retrieve the user id
	public static ArrayList<String> get_user_id(Context context,String email_id) {

		ArrayList<String> saved_info_list = new ArrayList<String>();
		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Cursor cu = db
				.rawQuery("SELECT " + DBContract.Content.user_id +" FROM " + DBContract.Content.PROFILE_DETAILS
						+ " where " + DBContract.Content.email + "='" + email_id + "'", null);

		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext())
		{
			if (cu.getString(0) != null && cu.getString(0) != "")
			{
				saved_info_list.add(cu.getString(0));
			}
		}
		cu.close();
		return saved_info_list;
	}


	//function to insert profile pic path for that id
	public static void insert_profile_pic_path(Context context,String pic,String user_id)
	{

		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Log.v(" test "," entered here ");
		// inserting into the PROFILE table
		ContentValues profile_save_info = new ContentValues();
		profile_save_info.put(DBContract.Content.profile_pic_path,pic);
		db.update(DBContract.Content.PROFILE_DETAILS, profile_save_info,
				DBContract.Content.user_id + "='" + user_id + "'", null);
	}
//
	//function to retrieve the user id
	public static ArrayList<String> get_profile_pic_path(Context context,String user_id) {

		ArrayList<String> saved_info_list = new ArrayList<String>();
		DBHelper dbHelper = new DBHelper(context); // call the constructor
		// //this will create the db
		// if it did not exist
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Cursor cu = db
				.rawQuery("SELECT " + DBContract.Content.profile_pic_path +" FROM " + DBContract.Content.PROFILE_DETAILS
						+ " where " + DBContract.Content.user_id + "='" + user_id + "'", null);

		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext())
		{
			if (cu.getString(0) != null && cu.getString(0) != "")
			{
				saved_info_list.add(cu.getString(0));
			}
		}
		cu.close();
		return saved_info_list;
	}
//	// function used to retrieve all the brands on refresh
//	public static ArrayList<String> get_saved_exam_dates(Context context) {
//
//		ArrayList<String> brand_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "0");
//		// db.update(DBContract.Content.BRAND_TABLE, updatedValues, null, null);
//
//		Cursor cu = db.rawQuery(
//				"SELECT Distinct " + DBContract.Content.brand_name + " FROM " + DBContract.Content.BRAND_TABLE, null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				brand_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return brand_array_list;
//	}
//
//	// function used to retrieve all the contents for the brand
//	public static ArrayList<String> get_all_contents(Context context, String brand_id) {
//
//		ArrayList<String> contents_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contents_name + " FROM "
//				+ DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.brand_id + "='" + brand_id + "'"
//		// + " AND "
//		// + DBContract.Content.is_deleted + "='" + deleted_status + "'"
//				, null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.contents_name + " FROM "
//		// + DBContract.Content.BRAND_TABLE
//		// , null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				contents_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return contents_array_list;
//	}
//
//	// function used to retrieve all the contents for the chosen brand on
//	// refresh
//	public static ArrayList<String> get_all_contents_on_refresh(Context context, String brand_id) {
//
//		ArrayList<String> contents_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "0");
//		// db.update(DBContract.Content.BRAND_TABLE, updatedValues, null, null);
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contents_name + " FROM "
//				+ DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.brand_id + "='" + brand_id + "'",
//				null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				contents_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return contents_array_list;
//	}
//
//	// function used to retrieve all the content for the chosen contents
//	public static ArrayList<String> get_all_content(Context context, String contents_id) {
//
//		ArrayList<String> content_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery(
//				"SELECT Distinct " + DBContract.Content.content_name + " FROM " + DBContract.Content.CONTENT_TABLE
//						+ " where " + DBContract.Content.contents_id + "='" + contents_id + "'"
//				// + " AND " + DBContract.Content.is_deleted + "='"
//				// + deleted_status + "'"
//				, null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				content_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return content_array_list;
//	}
//
//	// function used to retrieve all the content on refresh.
//	// Also update the delete status of the content.
//	public static ArrayList<String> get_all_content_on_refresh(Context context, String contents_id) {
//
//		ArrayList<String> content_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "0");
//		// db.update(DBContract.Content.CONTENT_TABLE, updatedValues, null,
//		// null);
//
//		Cursor cu = db.rawQuery(
//				"SELECT Distinct " + DBContract.Content.content_name + " FROM " + DBContract.Content.CONTENT_TABLE
//						+ " where " + DBContract.Content.contents_id + "='" + contents_id + "'",
//				null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				content_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return content_array_list;
//	}
//
//	// function used to retrieve all the content for the chosen contents
//	public static ArrayList<String> get_all_parent(Context context, String content_id) {
//
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.parent_name + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//		// + " AND " + DBContract.Content.is_deleted + "='"
//		// + deleted_status + "'"
//				, null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// function used to get all the parent urls in case parent names do not
//	// exist.
//	public static ArrayList<String> get_all_parent_url(Context context, String content_id) {
//
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contenturl_tag + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//		// + " AND " + DBContract.Content.is_deleted + "='"
//		// + deleted_status + "'"
//				, null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// function used to get the parent url considering pdfs.
//	public static ArrayList<String> get_all_parent_url_pdfs(Context context, String content_id) {
//
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.parent_name + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//		// + " AND " + DBContract.Content.is_deleted + "='"
//		// + deleted_status + "'"
//				, null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// function used to retrieve all the parent for the chosen content on
//	// refresh
//	public static ArrayList<String> get_all_parent_on_refresh(Context context, String content_id) {
//
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "0");
//		// db.update(DBContract.Content.PARENT_TABLE, updatedValues, null,
//		// null);
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.parent_name + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'",
//				null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// function used to get the brand id
//	public static String get_brand_id(Context context, String brand_name) {
//
//		String brandid = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.brand_id + " FROM " + DBContract.Content.BRAND_TABLE
//				+ " where " + DBContract.Content.brand_name + "='" + brand_name + "'", null);
//
//		if (cu.moveToFirst()) {
//			brandid = cu.getString(0);
//		}
//		cu.close();
//		return brandid;
//	}
//
//	// function used to get the contents_id
//	public static String get_contents_id(Context context, String contents_name) {
//
//		String contents_id = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.contents_id + " FROM " + DBContract.Content.BRAND_TABLE
//				+ " where " + DBContract.Content.contents_name + "='" + contents_name + "'", null);
//
//		if (cu.moveToFirst()) {
//			contents_id = cu.getString(0);
//		}
//		cu.close();
//		return contents_id;
//	}
//
//	// function used to get the content id
//	public static String get_content_id(Context context, String content_name, String contents_id) {
//
//		String content_id = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.content_id + " FROM " + DBContract.Content.CONTENT_TABLE
//				+ " where " + DBContract.Content.content_name + "='" + content_name + "'" + " AND "
//				+ DBContract.Content.contents_id + "='" + contents_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			content_id = cu.getString(0);
//		}
//		cu.close();
//		return content_id;
//	}
//
//	// function used to get the count for parent name for the chosen content
//	public static int get_count_of_parentName_for_content(Context context, String content_id) {
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.parent_name + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.parent_name + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				count = cu.getCount();
//			}
//
//		}
//		cu.close();
//
//		return (count);
//
//	}
//
//	// function used to get the count for parent name for the chosen content
//	public static int get_count_of_parentUrl_for_content(Context context, String content_id) {
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contenturl_tag + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.contenturl_tag + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				count = cu.getCount();
//			}
//
//		}
//		cu.close();
//
//		return (count);
//
//	}
//
//	// function used to get the parent id
//	public static String get_parent_id(Context context, String parent_name) {
//
//		String parent_id = null;
//		String empty_string = "";
//		String null_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.parent_id + " FROM " + DBContract.Content.PARENT_TABLE
//				+ " where " + DBContract.Content.parent_name + "='" + parent_name + "'", null);
//
//		if (cu.moveToFirst()) {
//			parent_id = cu.getString(0);
//		}
//		cu.close();
//		return parent_id;
//	}
//
//	// function used to get the parent id from content_id
//	public static String get_parent_id_from_content_id(Context context, String content_id) {
//
//		String parent_id = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.parent_id + " FROM " + DBContract.Content.PARENT_TABLE
//				+ " where " + DBContract.Content.content_id + "='" + content_id + "'" + " AND "
//				+ DBContract.Content.parent_id + "!='" + parent_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			parent_id = cu.getString(0);
//		}
//		cu.close();
//		return parent_id;
//	}
//
//	// function to get the url for contents
//	public static String get_contents_url(Context context, String contents_id) {
//
//		String contents_url = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		//
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.contents_url + " FROM " + DBContract.Content.BRAND_TABLE
//				+ " where " + DBContract.Content.contents_id + "='" + contents_id + "'" + " AND "
//				+ DBContract.Content.contents_url + "!='" + contents_url + "'", null);
//
//		// Cursor cu = db.rawQuery("SELECT " + DBContract.Content.contents_url +
//		// " FROM " + DBContract.Content.BRAND_TABLE
//		// , null);
//
//		if (cu.moveToFirst()) {
//			contents_url = cu.getString(0);
//		}
//		cu.close();
//		return contents_url;
//	}
//
//	// function to get the url for content
//	public static String get_content_url(Context context, String content_id) {
//
//		String content_url = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.cont_zip_url + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.cont_zip_url + "!='" + content_url + "'", null);
//
//		if (cu.moveToFirst()) {
//			content_url = cu.getString(0);
//		}
//		cu.close();
//		return content_url;
//	}
//
//	// function to get the url for parent
//	public static String get_parent_url(Context context, String parent_id) {
//
//		String parent_url = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.contenturl_tag + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.parent_id + "='" + parent_id + "'"
//				+ " AND " + DBContract.Content.contenturl_tag + "!='" + parent_url + "'", null);
//
//		if (cu.moveToFirst()) {
//			parent_url = cu.getString(0);
//		}
//		cu.close();
//		return parent_url;
//	}
//
//	// function to get content name from content id
//	public static String get_content_name_from_id(Context context, String content_id) {
//
//		String content_name = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.content_name + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.content_name + "!='" + content_name + "'", null);
//
//		if (cu.moveToFirst()) {
//			content_name = cu.getString(0);
//		}
//		cu.close();
//		return content_name;
//	}
//
//	// function to update the download status column for the parent.
//	public static boolean update_download_status_for_parent(Context context, String download_status, String parent_id,
//			String file_location)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		updatedValues.put(DBContract.Content.file_location, file_location);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//				DBContract.Content.parent_id + "='" + parent_id + "'", null) > 0);
//	}
//
//	// function to update the download status column for the parent when parent
//	// is a pdf
//	public static boolean update_download_status_for_parent_pdf(Context context, String download_status,
//			String file_location, String parent_pdf)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		updatedValues.put(DBContract.Content.file_location, file_location);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//				DBContract.Content.parent_name + "='" + parent_pdf + "'"
//				// + " AND " + DBContract.Content.content_id + "='"+ content_id
//				// + "'"
//				, null) > 0);
//	}
//
//	// function to update the download status for parent url
//	public static boolean update_download_status_for_parent_url(Context context, String download_status,
//			String file_location, String parent_url)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		updatedValues.put(DBContract.Content.file_location, file_location);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//				DBContract.Content.contenturl_tag + "='" + parent_url + "'"
//				// + " AND " + DBContract.Content.content_id + "='"+ content_id
//				// + "'"
//				, null) > 0);
//	}
//
//	// function to update the download status column for the content.
//	public static boolean update_download_status_for_content(Context context, String download_status, String content_id,
//			String file_location)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		updatedValues.put(DBContract.Content.cont_zip_path, file_location);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.CONTENT_TABLE, updatedValues,
//				DBContract.Content.content_id + "='" + content_id + "'", null) > 0);
//	}
//
//	// function to update the download status column for the contents.
//	public static boolean update_download_status_for_contents(Context context, String download_status,
//			String contents_id, String file_location)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		updatedValues.put(DBContract.Content.file_location, file_location);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.BRAND_TABLE, updatedValues,
//				DBContract.Content.contents_id + "='" + contents_id + "'", null) > 0);
//	}
//
//	// function to get the download status for the parent.
//	// Also get the file location for the parent.
//	public static ArrayList<String> get_download_status_for_parent(Context context, String parent_id) {
//
//		String empty_string = "";
//		String null_string = "";
//		ArrayList<String> d_f = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + "," + DBContract.Content.file_location
//				+ " FROM " + DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.parent_id + "='"
//				+ parent_id + "'" + " AND " + DBContract.Content.parent_name + "!='" + empty_string + "'" + " AND "
//				+ DBContract.Content.parent_name + "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			// parent_url = cu.getString(0);
//			d_f.add(cu.getString(0));
//			d_f.add(cu.getString(1));
//		}
//		cu.close();
//		return d_f;
//	}
//
//	// function to get the parent name and the parent file location for the
//	// particular parent url.
//	public static ArrayList<String> get_download_status_for_parent_url(Context context, String parent_url) {
//
//		// String parent_url = "";
//		ArrayList<String> d_f = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + "," + DBContract.Content.file_location
//				+ " FROM " + DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.contenturl_tag + "='"
//				+ parent_url + "'", null);
//
//		if (cu.moveToFirst()) {
//			// parent_url = cu.getString(0);
//			d_f.add(cu.getString(0));
//			d_f.add(cu.getString(1));
//		}
//		cu.close();
//		return d_f;
//	}
//
//	// function to get the download status for the parent.
//	// Also get the file location for the parent.
//	public static ArrayList<String> get_download_status_for_parent_pdf(Context context, String pdf_path) {
//
//		// String parent_url = "";
//		ArrayList<String> d_f = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + "," + DBContract.Content.file_location
//				+ " FROM " + DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.parent_name + "='"
//				+ pdf_path + "'", null);
//
//		if (cu.moveToFirst()) {
//			// parent_url = cu.getString(0);
//			d_f.add(cu.getString(0));
//			d_f.add(cu.getString(1));
//		}
//		cu.close();
//		return d_f;
//	}
//
//	// function to get the download status for the content.
//	// Also get the file location.
//	public static ArrayList<String> get_download_status_for_content(Context context, String content_id) {
//
//		ArrayList<String> d_f = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + "," + DBContract.Content.cont_zip_path
//				+ " FROM " + DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.content_id + "='"
//				+ content_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			d_f.add(cu.getString(0));
//			d_f.add(cu.getString(1));
//		}
//		cu.close();
//		return d_f;
//	}
//
//	// function to get the download status for the contents.
//	// Also return the file location.
//	public static ArrayList<String> get_download_status_for_contents(Context context, String contents_id) {
//
//		ArrayList<String> d_f = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + "," + DBContract.Content.file_location
//				+ " FROM " + DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.contents_id + "='"
//				+ contents_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			d_f.add(cu.getString(0));
//			d_f.add(cu.getString(1));
//		}
//		cu.close();
//		return d_f;
//	}
//
//	// function to get the download status for the brand
//	public static String get_download_status_for_brand(Context context, String brand_id) {
//
//		String parent_url = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + " FROM "
//				+ DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.brand_id + "='" + brand_id + "'",
//				null);
//
//		if (cu.moveToFirst()) {
//			parent_url = cu.getString(0);
//		}
//		cu.close();
//		return parent_url;
//	}
//
//	// function to get the file location for the parent
//	public static ArrayList<String> get_file_location_for_parent(Context context, String parent_id) {
//
//		String empty_string = "";
//		String null_string = null;
//		ArrayList<String> file_locations = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.file_location + ","
//				+ DBContract.Content.common_files_location + " FROM " + DBContract.Content.PARENT_TABLE + " where "
//				+ DBContract.Content.parent_id + "='" + parent_id + "'" + " AND " + DBContract.Content.file_location
//				+ "!='" + empty_string + "'" + " AND " + DBContract.Content.file_location + "!='" + null_string + "'",
//				null);
//
//		if (cu.moveToFirst()) {
//			file_locations.add(cu.getString(0));
//			file_locations.add(cu.getString(1));
//		}
//		cu.close();
//		return file_locations;
//	}
//
//	// function to get the file location for the content
//	public static ArrayList<String> get_file_location_for_content(Context context, String content_id) {
//
//		ArrayList<String> file_locations = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.cont_zip_path + ","
//				+ DBContract.Content.common_files_location + " FROM " + DBContract.Content.CONTENT_TABLE + " where "
//				+ DBContract.Content.content_id + "='" + content_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_locations.add(cu.getString(0));
//			file_locations.add(cu.getString(1));
//		}
//		cu.close();
//		return file_locations;
//	}
//
//	// function to get the file location for the contents
//	public static ArrayList<String> get_file_location_for_contents(Context context, String contents_id) {
//
//		String empty_string = "";
//		String null_string = null;
//		ArrayList<String> file_locations = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.file_location + " FROM " + DBContract.Content.BRAND_TABLE
//				+ " where " + DBContract.Content.contents_id + "='" + contents_id + "'" + " AND "
//				+ DBContract.Content.file_location + "!='" + empty_string + "'" + " AND "
//				+ DBContract.Content.file_location + "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_locations.add(cu.getString(0));
//		}
//		cu.close();
//		return file_locations;
//	}
//
//	// function used to delete the entries from the database for the chosen
//	// brand
//	public static boolean delete_brand_entries(Context context, String string) {
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		return db.delete(DBContract.Content.BRAND_TABLE, DBContract.Content.brand_id + "='" + string + "'", null) > 0;
//		//
//		// ContentValues updatedValues = new ContentValues();
//		// // updatedValues.put(DBContract.Content.is_deleted, "1");
//		// return (db.update(DBContract.Content.BRAND_TABLE, updatedValues,
//		// DBContract.Content.brand_id + "='" + string + "'", null) > 0);
//	}
//
//	// function used to delete the entries from the database for the chosen
//	// contents
//	public static boolean delete_contents_entries(Context context, String string) {
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		db.delete(DBContract.Content.CONTENT_TABLE, DBContract.Content.contents_id + "='" + string + "'", null);
//		return db.delete(DBContract.Content.BRAND_TABLE, DBContract.Content.contents_id + "='" + string + "'",
//				null) > 0;
//
//		// ContentValues updatedValues = new ContentValues();
//		// // updatedValues.put(DBContract.Content.is_deleted, "1");
//		// updatedValues.put(DBContract.Content.download_status, "0");
//		// return (db.update(DBContract.Content.BRAND_TABLE, updatedValues,
//		// DBContract.Content.contents_id + "='" + string + "'", null) > 0);
//	}
//
//	// function used to delete the entries from the database for the chosen
//	// content
//	public static boolean delete_content_entries(Context context, String string) {
//
//		Log.v(" test ", " into delete content entries function " + string);
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		db.delete(DBContract.Content.PARENT_TABLE, DBContract.Content.content_id + "='" + string + "'", null);
//		return db.delete(DBContract.Content.CONTENT_TABLE, DBContract.Content.content_id + "='" + string + "'",
//				null) > 0;
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "1");
//		// updatedValues.put(DBContract.Content.download_status, "0");
//		// return (db.update(DBContract.Content.CONTENT_TABLE, updatedValues,
//		// DBContract.Content.content_id + "='" + string + "'", null) > 0);
//	}
//
//	// function used to delete the entries from the database for the chosen
//	// parent
//	public static boolean delete_parent_entries(Context context, String string) {
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		return db.delete(DBContract.Content.PARENT_TABLE, DBContract.Content.parent_id + "='" + string + "'", null) > 0;
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "1");
//		// updatedValues.put(DBContract.Content.download_status, "0");
//		// return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//		// DBContract.Content.parent_id + "='" + string + "'", null) > 0);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_brand_id(Context context, String brand_id) {
//
//		Log.v(" test ", " inserting brand id ");
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues BRANDtableContent = new ContentValues();
//		BRANDtableContent.put(DBContract.Content.brand_id, brand_id);
//		// BRANDtableContent.put(DBContract.Content.is_deleted, "0");
//		// BRANDtableContent.put(DBContract.Content.download_status, "0");
//		// BRANDtableContent.put(DBContract.Content.file_location, "");
//
//		db.insert(DBContract.Content.BRAND_TABLE, null, BRANDtableContent);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_content_id(Context context, String content_id) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the CONTENT table
//		ContentValues CONTENTtableContent = new ContentValues();
//		CONTENTtableContent.put(DBContract.Content.content_id, content_id);
//		db.insert(DBContract.Content.CONTENT_TABLE, null, CONTENTtableContent);
//		db.insert(DBContract.Content.PARENT_TABLE, null, CONTENTtableContent);
//		//
//		// // inserting into the PARENT table
//		// ContentValues PARENTtableContent = new ContentValues();
//		// PARENTtableContent.put(DBContract.Content.content_id, content_id);
//		// db.insert(DBContract.Content.PARENT_TABLE, null, PARENTtableContent);
//
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_parent_name(Context context, String string1, String string2, String string3) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.parent_name, string1);
//		table_content.put(DBContract.Content.parent_id, string2);
//		table_content.put(DBContract.Content.content_id, string3);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		db.insert(DBContract.Content.PARENT_TABLE, null, table_content);
//	}
//
//	// function used to insert the parent pdf file path in the parent table.
//	public static void insert_parent_pdf_path(Context context, String string1, String string2, String string3,
//			String string4) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.parent_name, string1);
//		table_content.put(DBContract.Content.parent_id, string2);
//		table_content.put(DBContract.Content.content_id, string3);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		table_content.put(DBContract.Content.cat_parent_id, string4);
//		db.insert(DBContract.Content.PARENT_TABLE, null, table_content);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_parent_id(Context context, String string) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the CONTENT table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.parent_id, string);
//		db.insert(DBContract.Content.PARENT_TABLE, null, table_content);
//
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_contents_id(Context context, String string) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.contents_id, string);
//		db.insert(DBContract.Content.BRAND_TABLE, null, table_content);
//		//db.insert(DBContract.Content.CONTENT_TABLE, null, table_content);
//
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_root_directory(Context context, String string1, String brand_id) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.root_folder, string1);
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		db.update(DBContract.Content.BRAND_TABLE, table_content, DBContract.Content.brand_id + "='" + brand_id + "'",
//				null);
//	}
//
//	// function used to get the root directory
//	public static String get_main_root_directory(Context context, String brand_id) {
//
//		String root_name = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.root_folder + " FROM " + DBContract.Content.BRAND_TABLE
//				+ " where " + DBContract.Content.root_folder + "!='" + root_name + "'" + " AND "
//				+ DBContract.Content.brand_id + "='" + brand_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			root_name = cu.getString(0);
//		}
//		cu.close();
//		return root_name;
//	}
//
//	// function used to get the content zip url
//	public static String get_content_zip_url(Context context, String content_id) {
//
//		String content_zip_path = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.cont_zip_url + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.cont_zip_url + "!='"
//				+ content_zip_path + "'" + " AND " + DBContract.Content.content_id + "='" + content_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			content_zip_path = cu.getString(0);
//		}
//		cu.close();
//		return content_zip_path;
//	}
//
//	// function to get the file location for the parent based on content id.
//	public static ArrayList<String> get_file_location_for_parent_from_content_id(Context context, String content_id) {
//
//		String empty_string = "";
//		String null_string = null;
//		ArrayList<String> file_locations = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.file_location + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.file_location + "!='" + empty_string + "'" + " AND "
//				+ DBContract.Content.file_location + "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_locations.add(cu.getString(0));
//		}
//		cu.close();
//		return file_locations;
//	}
//
//	// function to get the file location for the content based on content id
//	public static ArrayList<String> get_file_location_for_content_from_content_id(Context context, String content_id) {
//
//		String empty_string = "";
//		String null_string = null;
//		ArrayList<String> file_locations = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.cont_zip_path + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.cont_zip_path + "!='" + empty_string + "'" + " AND "
//				+ DBContract.Content.cont_zip_path + "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_locations.add(cu.getString(0));
//		}
//		cu.close();
//		return file_locations;
//	}
//
//	// function to update the download status referring to the content id.
//	public static boolean update_download_status_for_parent_from_content_id(Context context, String download_status,
//			String content_id, String file_location)
//
//	{
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		updatedValues.put(DBContract.Content.file_location, file_location);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//				DBContract.Content.content_id + "='" + content_id + "'", null) > 0);
//	}
//
//	// function to get the parent_id from parent url
//	public static String get_parent_id_from_parent_url(Context context, String parent_name) {
//
//		String parent_id = null;
//		String empty_string = "";
//		String null_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.parent_id + " FROM " + DBContract.Content.PARENT_TABLE
//				+ " where " + DBContract.Content.contenturl_tag + "='" + parent_name + "'" + " AND "
//				+ DBContract.Content.parent_id + "!='" + empty_string + "'" + " AND " + DBContract.Content.parent_id
//				+ "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			parent_id = cu.getString(0);
//		}
//		cu.close();
//		return parent_id;
//	}
//
//	// function to get content_id from content zip url
//	public static String get_content_id_from_url(Context context, String content_url) {
//
//		String content_id = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.content_id + " FROM " + DBContract.Content.CONTENT_TABLE
//				+ " where " + DBContract.Content.cont_zip_url + "='" + content_url + "'" + " AND "
//				+ DBContract.Content.content_id + "!='" + content_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			content_id = cu.getString(0);
//		}
//		cu.close();
//		return content_id;
//	}
//
//	// function used to retrieve all the content ids for the contents id
//	public static ArrayList<String> get_all_content_id_from_contents_id(Context context, String contents_id) {
//
//		String empty_string = "";
//		String null_string = null;
//		ArrayList<String> content_id_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.content_id + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.contents_id + "='" + contents_id
//				+ "'" + " AND " + DBContract.Content.content_id + "!='" + null_string + "'" + " AND "
//				+ DBContract.Content.content_id + "!='" + empty_string + "'", null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				content_id_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return content_id_array_list;
//	}
//
//	// function used to get the zip url based on contents id
//	public static ArrayList<String> get_content_zip_url_from_contents_id(Context context, String content_id) {
//
//		ArrayList<String> content_zip_url_list = new ArrayList<String>();
//		String content_zip_path = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.cont_zip_url + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.cont_zip_url + "!='"
//				+ content_zip_path + "'" + " AND " + DBContract.Content.contents_id + "='" + content_id + "'", null);
//
//		if (cu.moveToFirst()) {
//			content_zip_url_list.add(cu.getString(0));
//		}
//		cu.close();
//		return content_zip_url_list;
//	}
//
//	// function to get the parent name from parent id
//	public static String get_parent_name_from_parent_id(Context context, String parent_id) {
//
//		String parent_name = null;
//		String empty_string = "";
//		String null_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.parent_name + " FROM " + DBContract.Content.PARENT_TABLE
//				+ " where " + DBContract.Content.parent_id + "='" + parent_id + "'" + " AND "
//				+ DBContract.Content.parent_name + "!='" + empty_string + "'" + " AND " + DBContract.Content.parent_name
//				+ "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			parent_name = cu.getString(0);
//		}
//		cu.close();
//		return parent_name;
//	}
//
//	// function used to delete the entries from the database for the chosen
//	// parent
//	public static boolean delete_parent_entries_from_parent_name(Context context, String string, String string2) {
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		return db.delete(DBContract.Content.PARENT_TABLE, DBContract.Content.parent_name + "='" + string + "'" + " AND "
//				+ DBContract.Content.parent_id + "='" + string2 + "'", null) > 0;
//
//		// ContentValues updatedValues = new ContentValues();
//		// //updatedValues.put(DBContract.Content.is_deleted, "1");
//		// updatedValues.put(DBContract.Content.download_status, "0");
//		// return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//		// DBContract.Content.parent_id + "='" + string + "'", null) > 0);
//	}
//
//	// function to get the count of downloaded items
//	public static int get_count_of_parentName_download_for_content(Context context, String content_id,
//			String cont_name) {
//		String null_string = null;
//		String empty_string = "";
//		String status = "1";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.download_status + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.download_status + "='" + status + "'"
//				+ " AND " + DBContract.Content.content_id + "!='" + content_id + "'" + " AND "
//				+ DBContract.Content.content_name + "!='" + cont_name + "'", null);
//
//		if (cu.moveToFirst()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				count = cu.getCount();
//			}
//
//		}
//		cu.close();
//
//		return (count);
//
//	}
//
//	// get all parent for a chosen content id where the download status is 1;
//	public static ArrayList<String> get_all_downloaded_parent(Context context, String content_id) {
//
//		String status = "1";
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.parent_name + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.download_status + "='" + status + "'", null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// get all parent for a chosen content id where the download status is 1;
//	public static ArrayList<String> get_all_downloaded_parent_url(Context context, String content_id) {
//
//		String status = "1";
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contenturl_tag + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.download_status + "='" + status + "'", null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// function used to get the count for content name for the chosen contents
//	public static int get_count_of_contentName_for_contents(Context context, String contents_id) {
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.content_name + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.contents_id + "='" + contents_id
//				+ "'" + " AND " + DBContract.Content.content_name + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				count = cu.getCount();
//			}
//
//		}
//		cu.close();
//
//		return (count);
//
//	}
//
//	// get all content for whom the download status is 1
//	public static ArrayList<String> get_all_downloaded_content(Context context, String contents_id) {
//
//		String status = "1";
//		ArrayList<String> parent_array_list = new ArrayList<String>();
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.content_name + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.contents_id + "='" + contents_id
//				+ "'" + " AND " + DBContract.Content.download_status + "='" + status + "'", null);
//
//		// Cursor cu = db.rawQuery("SELECT Distinct " +
//		// DBContract.Content.content_id + " FROM "
//		// + DBContract.Content.PARENT_TABLE ,
//		// null);
//
//		for (cu.moveToFirst(); !cu.isAfterLast(); cu.moveToNext()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				parent_array_list.add(cu.getString(0));
//			}
//
//		}
//		cu.close();
//		return parent_array_list;
//	}
//
//	// function used to check if a brand id exist.
//	public static int check_if_brand_id_exist(Context context, String brand_id) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.brand_id + " FROM "
//				+ DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.brand_id + "='" + brand_id + "'",
//				null);
//
//		count = cu.getCount();
//
//		//
//		// if (cu.moveToFirst()) {
//		// if (cu.getString(0) != null && cu.getString(0) != "") {
//		// count = cu.getCount();
//		// }
//		//
//		// }
//		cu.close();
//		return count;
//	}
//
//	// function used to check if a parent id exist.
//	public static int check_if_parent_id_exist(Context context, String parent_id) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.parent_id + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.parent_id + "='" + parent_id + "'",
//				null);
//
//		count = cu.getCount();
//
//		//
//		// if (cu.moveToFirst()) {
//		// if (cu.getString(0) != null && cu.getString(0) != "") {
//		// count = cu.getCount();
//		// }
//		//
//		// }
//		cu.close();
//		return count;
//	}
//
//	// function used to check if a content id exist
//	public static int check_if_content_id_exist(Context context, String content_id) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		int count = 0;
//		Cursor cu = db.rawQuery(
//				"SELECT Distinct " + DBContract.Content.content_id + " FROM " + DBContract.Content.CONTENT_TABLE
//						+ " where " + DBContract.Content.content_id + "='" + content_id + "'",
//				null);
//
//		count = cu.getCount();
//
//		//
//		// if (cu.moveToFirst()) {
//		// if (cu.getString(0) != null && cu.getString(0) != "") {
//		// count = cu.getCount();
//		// }
//		//
//		// }
//		cu.close();
//		return count;
//	}
//
//	// function used to check if contents id exist
//	public static int check_if_contents_id_exist(Context context, String contents_id) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		int count = 0;
//		Cursor cu = db.rawQuery(
//				"SELECT Distinct " + DBContract.Content.contents_id + " FROM " + DBContract.Content.BRAND_TABLE
//						+ " where " + DBContract.Content.contents_id + "='" + contents_id + "'",
//				null);
//
//		count = cu.getCount();
//
//		//
//		// if (cu.moveToFirst()) {
//		// if (cu.getString(0) != null && cu.getString(0) != "") {
//		// count = cu.getCount();
//		// }
//		//
//		// }
//		cu.close();
//		return count;
//	}
//
//	// function to update the download status column for the contents.
//	public static boolean update_download_status_for_contents_based_on_id(Context context, String contents_id,
//			String download_status)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.BRAND_TABLE, updatedValues,
//				DBContract.Content.contents_id + "='" + contents_id + "'", null) > 0);
//	}
//
//	// function used to get file location based on chosen parent name
//	public static String get_file_location_from_parent_name(Context context, String parent_name) {
//
//		String file_loc = null;
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.file_location + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.parent_name + "='" + parent_name
//				+ "'" + " AND " + DBContract.Content.file_location + "!='" + null_string + "'" + " AND "
//				+ DBContract.Content.file_location + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_loc = cu.getString(0);
//		}
//		cu.close();
//		return file_loc;
//	}
//
//	// function used to get file location based on chosen parent name's
//	// contenturl tag
//	public static String get_file_location_from_parent_name_contenturl(Context context, String content_url_tag) {
//
//		String file_loc = null;
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.file_location + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.contenturl_tag + "='"
//				+ content_url_tag + "'" + " AND " + DBContract.Content.file_location + "!='" + null_string + "'"
//				+ " AND " + DBContract.Content.file_location + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_loc = cu.getString(0);
//		}
//		cu.close();
//		return file_loc;
//	}
//
//	// get the content url tag from parent id
//	public static String get_content_url_from_parent_id(Context context, String parent_id) {
//
//		String file_loc = null;
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.contenturl_tag + " FROM "
//				+ DBContract.Content.PARENT_TABLE + " where " + DBContract.Content.parent_id + "='" + parent_id + "'"
//				+ " AND " + DBContract.Content.contenturl_tag + "!='" + null_string + "'" + " AND "
//				+ DBContract.Content.contenturl_tag + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_loc = cu.getString(0);
//		}
//		cu.close();
//		return file_loc;
//	}
//
//	// function to update download status for content on content id
//	public static boolean update_download_status_for_content_based_on_id(Context context, String content_id,
//			String download_status)
//
//	{
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		ContentValues updatedValues = new ContentValues();
//		updatedValues.put(DBContract.Content.download_status, download_status);
//		// updatedValues.put(DBContract.Content.common_files_location,
//		// common_file_location);
//		return (db.update(DBContract.Content.CONTENT_TABLE, updatedValues,
//				DBContract.Content.content_id + "='" + content_id + "'", null) > 0);
//	}
//
//	// get the file location for the content based on the contentname
//	public static String get_content_location_based_on_contentname(Context context, String content_name) {
//
//		String file_loc = null;
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.cont_zip_path + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.content_name + "='" + content_name
//				+ "'" + " AND " + DBContract.Content.cont_zip_path + "!='" + null_string + "'" + " AND "
//				+ DBContract.Content.cont_zip_path + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_loc = cu.getString(0);
//		}
//		cu.close();
//		return file_loc;
//	}
//
//	// get content id based on the parent url downloaded
//	public static String get_content_id_from_contenturl(Context context, String contenturl) {
//
//		String file_loc = null;
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.content_id + " FROM " + DBContract.Content.CONTENT_TABLE
//				+ " where " + DBContract.Content.contenturl_tag + "='" + contenturl + "'" + " AND "
//				+ DBContract.Content.content_id + "!='" + null_string + "'" + " AND " + DBContract.Content.content_id
//				+ "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_loc = cu.getString(0);
//		}
//		cu.close();
//		return file_loc;
//	}
//
//	// function used to get the contents id from brand id
//	public static ArrayList<String> get_contents_id_from_brand_id(Context context, String brand_id) {
//		ArrayList<String> contents_ids = new ArrayList<String>();
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.contents_id + " FROM " + DBContract.Content.BRAND_TABLE
//				+ " where " + DBContract.Content.brand_id + "='" + brand_id + "'" + " AND "
//				+ DBContract.Content.contents_id + "!='" + null_string + "'" + " AND " + DBContract.Content.contents_id
//				+ "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			contents_ids.add(cu.getString(0));
//		}
//		cu.close();
//		return contents_ids;
//	}
//
//	// added on 27-10-2016
//	// function used to get datetime from content id
//	public static String get_datetime_from_content_id(Context context, String content_id) {
//		String datetime_stamp = "";
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.time_stamp + " FROM " + DBContract.Content.CONTENT_TABLE
//				+ " where " + DBContract.Content.content_id + "='" + content_id + "'" + " AND "
//				+ DBContract.Content.time_stamp + "!='" + null_string + "'" + " AND " + DBContract.Content.time_stamp
//				+ "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			datetime_stamp = cu.getString(0);
//		}
//		cu.close();
//		return datetime_stamp;
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_date_time(Context context, String string1, String string2) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.time_stamp, string1);
//		table_content.put(DBContract.Content.content_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.cont_zip_path, "");
//		// db.insert(DBContract.Content.CONTENT_TABLE, null, table_content);
//		db.update(DBContract.Content.CONTENT_TABLE, table_content, DBContract.Content.content_id + "='" + string2 + "'",
//				null);
//		//
//		// return (db.update(DBContract.Content.PARENT_TABLE, updatedValues,
//		// DBContract.Content.parent_id + "='" + parent_id + "'", null) > 0);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_content_name(Context context, String string1, String string2, String string3) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.content_name, string1);
//		table_content.put(DBContract.Content.content_id, string2);
//		table_content.put(DBContract.Content.contents_id, string3);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.cont_zip_path, "");
//		// db.insert(DBContract.Content.CONTENT_TABLE, null, table_content);
//		db.update(DBContract.Content.CONTENT_TABLE, table_content, DBContract.Content.content_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_content_type(Context context, String string1, String string2) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.content_type, string1);
//		table_content.put(DBContract.Content.content_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.cont_zip_path, "");
//		// db.insert(DBContract.Content.CONTENT_TABLE, null, table_content);
//		db.update(DBContract.Content.CONTENT_TABLE, table_content, DBContract.Content.content_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void inserting_zip_url(Context context, String string1, String string2) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.cont_zip_url, string1);
//		table_content.put(DBContract.Content.content_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.cont_zip_path, "");
//		// db.insert(DBContract.Content.CONTENT_TABLE, null, table_content);
//		db.update(DBContract.Content.CONTENT_TABLE, table_content, DBContract.Content.content_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void inserting_delete_status(Context context, String string1, String string2) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.delete_ids, string1);
//		table_content.put(DBContract.Content.content_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.cont_zip_path, "");
//		// db.insert(DBContract.Content.CONTENT_TABLE, null, table_content);
//		db.update(DBContract.Content.CONTENT_TABLE, table_content, DBContract.Content.content_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_brand_name(Context context, String brand_id, String brand_name) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Log.v(" test ", " inserting brand name " + brand_name + " for id " + brand_id);
//		// inserting into the BRAND table
//		ContentValues BRANDtableContent = new ContentValues();
//		BRANDtableContent.put(DBContract.Content.brand_name, brand_name);
//		BRANDtableContent.put(DBContract.Content.brand_id, brand_id);
//		// BRANDtableContent.put(DBContract.Content.is_deleted, "0");
//		// BRANDtableContent.put(DBContract.Content.download_status, "0");
//		// BRANDtableContent.put(DBContract.Content.file_location, "");
//		// db.insert(DBContract.Content.BRAND_TABLE, null, BRANDtableContent);
//		db.update(DBContract.Content.BRAND_TABLE, BRANDtableContent,
//				DBContract.Content.brand_id + "='" + brand_id + "'", null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_brand_logo(Context context, String brand_id, String brand_logo) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues BRANDtableContent = new ContentValues();
//		BRANDtableContent.put(DBContract.Content.brand_image_uri, brand_logo);
//		BRANDtableContent.put(DBContract.Content.brand_id, brand_id);
//		// BRANDtableContent.put(DBContract.Content.is_deleted, "0");
//		// BRANDtableContent.put(DBContract.Content.download_status, "0");
//		// BRANDtableContent.put(DBContract.Content.file_location, "");
//		// db.insert(DBContract.Content.BRAND_TABLE, null, BRANDtableContent);
//		db.update(DBContract.Content.BRAND_TABLE, BRANDtableContent,
//				DBContract.Content.brand_id + "='" + brand_id + "'", null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void inserting_contents_name(Context context, String string1, String string2, String string3) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.contents_name, string1);
//		table_content.put(DBContract.Content.contents_id, string2);
//		table_content.put(DBContract.Content.brand_id, string3);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		// db.insert(DBContract.Content.BRAND_TABLE, null, table_content);
//		db.update(DBContract.Content.BRAND_TABLE, table_content, DBContract.Content.contents_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void inserting_contents_logo(Context context, String string1, String string2) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.contents_logo, string1);
//		table_content.put(DBContract.Content.contents_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		// db.insert(DBContract.Content.BRAND_TABLE, null, table_content);
//		db.update(DBContract.Content.BRAND_TABLE, table_content, DBContract.Content.contents_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void inserting_contents_url(Context context, String string1, String string2) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Log.v(" test ", " inserting contents url " + string1 + "   andd " + string2);
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.contents_url, string1);
//		table_content.put(DBContract.Content.contents_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		// db.insert(DBContract.Content.BRAND_TABLE, null, table_content);
//		db.update(DBContract.Content.BRAND_TABLE, table_content, DBContract.Content.contents_id + "='" + string2 + "'",
//				null);
//	}
//
//	// function used to insert the notations in the notation table
//	public static void insert_parent_url(Context context, String string1, String string2, String string3) {
//
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//													// //this will create the db
//													// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		// inserting into the BRAND table
//		ContentValues table_content = new ContentValues();
//		table_content.put(DBContract.Content.contenturl_tag, string1);
//		table_content.put(DBContract.Content.parent_id, string2);
//		// table_content.put(DBContract.Content.is_deleted, "0");
//		// table_content.put(DBContract.Content.download_status, "0");
//		// table_content.put(DBContract.Content.file_location, "");
//		table_content.put(DBContract.Content.content_id, string3);
//		// db.insert(DBContract.Content.PARENT_TABLE, null, table_content);
//		db.update(DBContract.Content.PARENT_TABLE, table_content, DBContract.Content.parent_id + "='" + string2 + "'",
//				null);
//	}
//
//	// get content id based on the parent url downloaded
//	public static String get_content_type_from_id(Context context, String content_id) {
//
//		String file_loc = null;
//		String null_string = null;
//		String empty_string = "";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//
//		Cursor cu = db.rawQuery("SELECT " + DBContract.Content.content_type + " FROM "
//				+ DBContract.Content.CONTENT_TABLE + " where " + DBContract.Content.content_id + "='" + content_id + "'"
//				+ " AND " + DBContract.Content.content_type + "!='" + null_string + "'" + " AND "
//				+ DBContract.Content.content_type + "!='" + empty_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			file_loc = cu.getString(0);
//		}
//		cu.close();
//		return file_loc;
//	}
//
//	// added on 11-11-2016
//	// function used to get the count for parent name for the chosen content
//	public static int get_contents_count_for_brand(Context context, String brand_id) {
//		String empty_string = "";
//		String null_string = null;
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contents_name + " FROM "
//				+ DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.brand_id + "='" + brand_id + "'"
//				+ " AND " + DBContract.Content.contents_name + "!='" + empty_string + "'" + " AND "
//				+ DBContract.Content.contents_name + "!='" + null_string + "'", null);
//
//		if (cu.moveToFirst()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				count = cu.getCount();
//			}
//
//		}
//		cu.close();
//
//		return (count);
//
//	}
//
//	// function used to get the count for parent name for the chosen content
//	public static int get_download_contents_count_for_brand(Context context, String brand_id) {
//		String empty_string = "";
//		String null_string = null;
//		String download_status = "1";
//		DBHelper dbHelper = new DBHelper(context); // call the constructor
//		// //this will create the db
//		// if it did not exist
//		SQLiteDatabase db = dbHelper.getWritableDatabase();
//		int count = 0;
//		Cursor cu = db.rawQuery("SELECT Distinct " + DBContract.Content.contents_name + " FROM "
//				+ DBContract.Content.BRAND_TABLE + " where " + DBContract.Content.brand_id + "='" + brand_id + "'"
//				+ " AND " + DBContract.Content.contents_name + "!='" + empty_string + "'" + " AND "
//				+ DBContract.Content.contents_name + "!='" + null_string + "'" + " AND "
//				+ DBContract.Content.download_status + "='" + download_status + "'", null);
//
//		if (cu.moveToFirst()) {
//			if (cu.getString(0) != null && cu.getString(0) != "") {
//				count = cu.getCount();
//			}
//
//		}
//		cu.close();
//
//		return (count);
//
//	}
}