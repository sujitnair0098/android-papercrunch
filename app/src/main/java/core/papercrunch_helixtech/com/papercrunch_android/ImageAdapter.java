package core.papercrunch_helixtech.com.papercrunch_android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by helixtech-android on 28/11/16.
 */
public class ImageAdapter extends BaseAdapter {

    Papers_option_Fragment my_fragment;
    Context mContext;
    private String[] title;
    public ImageAdapter(Context c, String[] web,Papers_option_Fragment fragment)
    {
        mContext = c;
        this.title = web;
        my_fragment = fragment;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return title[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        PostValue.set_current_paper_layout("subjects");
        View v = null;
        final ViewHolder viewholder;
        if (convertView == null) { // if it's not recycled, initialize some
            // attributes
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.gridview_layout, parent, false);
        } else {
            v = (View) convertView;
        }

        viewholder = new ViewHolder();
        viewholder.textView = (TextView) v.findViewById(R.id.grid_text);
        viewholder.sub_layout = (LinearLayout) v.findViewById(R.id.subject_layout);
        viewholder.textView.setText(title[position]);

        viewholder.sub_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                my_fragment.refresh("test");
            }
        });
        return v;

    }

//    public void show_no_connection_dialog() {
//        PostValue.set_download_cancelled(1);
//        PostValue.set_download_in_progress(0);
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
//
//        // set title
//        alertDialogBuilder.setTitle("Alert.");
//
//        // set dialog message
//        alertDialogBuilder.setMessage("Cannot Download. No network connection.").setCancelable(false)
//                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // if this button is clicked, just close
//                        // the dialog box and do nothing
//                        dialog.cancel();
//
//                    }
//                });
//
//        // create alert dialog
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        // show it
//        alertDialog.show();
//    }


    public void dataChange(String[] passed_array) {
        this.title = passed_array;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        TextView textView;
        LinearLayout sub_layout;
    }

}