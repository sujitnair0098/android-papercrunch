package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Created by helixtech-android on 12/11/16.
 */
public class Profile_Fragment extends Fragment {

    ProgressDialog progress;
    //Button logout;
    TextView full_name;
    String pic_url="",firstName="",lastName="";

    ImageView profile_pic;

    Bitmap thumbnail;
    private final int PICTURE_RESULT = 20;
    private Uri imageUri;
    String imageurl;

    TextView college,branch_semester,email;

    Button settings_button;

    public Profile_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.you_fragment,
                container, false);

        //logout = (Button) view.findViewById(R.id.logout_button);
        full_name = (TextView) view.findViewById(R.id.full_name);
        profile_pic = (ImageView) view.findViewById(R.id.profile_picture);
        college = (TextView) view.findViewById(R.id.colg_name);
        branch_semester = (TextView) view.findViewById(R.id.branch_and_sem);
        email = (TextView) view.findViewById(R.id.email_id);
        settings_button = (Button) view.findViewById(R.id.profile_settings);

        settings_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // custom dialog
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.profile_settings_layout);
                dialog.setTitle("Profile");

                // set the custom dialog components - text, image and button
                ListView option_list = (ListView) dialog.findViewById(R.id.options_list);

                option_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                    {
                        if(i == 5)
                        {
                            String MY_PREFS_NAME="mysharedpreferences";
                            SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
                            editor.putString("username",null);
                            editor.putString("password",null);

                            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);
                            if(prefs.getString("facebooklogin",null) != null)
                            {
                                if(prefs.getString("facebooklogin",null).equals("1"))
                                {
                                    editor.putString("facebooklogin","0");
                                }
                            }

                            editor.commit();

                            getActivity().finish();
                            Intent open_welcome_activity = new Intent(getActivity(),WelcomeActivity.class);
                            getActivity().startActivity(open_welcome_activity);
                        }
                    }
                });
                TextView cancel = (TextView) dialog.findViewById(R.id.cancel_textview);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                String[] subjects = {
                        "Share",
                        "Edit Profile",
                        "Settings",
                        "About",
                        "Contact Us",
                        "Logout"
                } ;

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,
                        android.R.id.text1,subjects);

                option_list.setAdapter(adapter);

//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog.cancel();
//                    }
//                });
                dialog.show();
            }
        });

        ArrayList<String> academic_details = DBOps.get_saved_academic_details(getContext(),PostValue.get_current_user_id());
        //The order in which data is received is as follows.
        //0-college name
        //1-branch
        //2-semester
        //3-elective
        //4-backlog subjects.

        if(academic_details.size() > 0) {
            String result = academic_details.get(0);
            Log.v(" test "," result is "+result);
            college.setText(result.substring(0, result.indexOf("*")));

            result = result.substring(result.indexOf("*") + 1);
            String branch = result.substring(0, result.indexOf("*"));
            result = result.substring(result.indexOf("*") + 1);
            String sem = result.substring(0, result.indexOf("*"));
            branch_semester.setText(branch + ", Semester " + sem);
        }

        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //requesting permission on runtime to handle the android marshmallow version.
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1) {
                    String[] perms = {"android.permission.CAMERA"};
                    int permsRequestCode = 200;
                    requestPermissions(perms, permsRequestCode);
                }
                else
                {
                    open_camera_intent();
                }


            }
        });

//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String MY_PREFS_NAME="mysharedpreferences";
//                SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
//                editor.putString("username",null);
//                editor.putString("password",null);
//                editor.commit();
//
//                getActivity().finish();
//                Intent open_welcome_activity = new Intent(getActivity(),WelcomeActivity.class);
//                getActivity().startActivity(open_welcome_activity);
//
//            }
//        });

        //get the current user first_name and last_name

////        Log.v(" test "," current user id is "+PostValue.get_current_user_id());
//        ArrayList<String> user_name = DBOps.get_saved_profile_details(getContext(),PostValue.get_current_user_id());
//
//        if(user_name.size() > 0)
//        {
//            String result = user_name.get(0);
//            firstName = result.substring(0,result.indexOf("*"));
//            result = result.substring(result.indexOf("*")+1);
//            lastName = result.substring(0,result.indexOf("*"));
//            result = result.substring(result.indexOf("*")+1);
//            email.setText(result.substring(0,result.indexOf("*")));
//

        Log.v(" test "," the id is "+PostValue.get_current_user_id());
            ArrayList<String> profile_path = DBOps.get_profile_pic_path(getContext(),PostValue.get_current_user_id());
            if(profile_path.size() > 0) {
                pic_url = profile_path.get(0);
            }
            else
            {
                pic_url="";
            }
//        }


        String MY_PREFS_NAME="mysharedpreferences";
        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);

        if(prefs.getString("facebooklogin",null) != null) {
            if (prefs.getString("facebooklogin", null).equals("1")) {
                pic_url = prefs.getString("profile_pic_url", null);
            }
        }
        firstName = prefs.getString("firstname",null);
        lastName = prefs.getString("lastname",null);
        email.setText(prefs.getString("username",null));
        full_name.setText(firstName+" "+lastName);
        Log.v(" test "," the pic url is "+pic_url);
        if(pic_url != null && pic_url != "")
        {
            String temp;
            if(pic_url.contains(":"))
            {
                temp=pic_url.substring(0,pic_url.indexOf(":"));
                if(temp.equals("https") || temp.equals("http"))
                {
                    progress = ProgressDialog.show(getActivity(), "Loading",
                            "", true);
                    new ProgressBack().execute();
                }
            }
            else
            {
               Log.v(" test "," the file location is"+pic_url);
                File imgFile = new  File(pic_url);
                if(imgFile.exists()){

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    profile_pic.setImageBitmap(getCircleBitmap(myBitmap));

                }
            }

        }

        return view;
    }


    public void open_camera_intent()
    {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getActivity().getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PICTURE_RESULT);
        //getActivity().startActivityForResult(intent,111);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {

            case PICTURE_RESULT:
                if (requestCode == PICTURE_RESULT)
                {

                    Log.v(" test "," save here ");
                    if (resultCode == Activity.RESULT_OK)
                    {
                        try
                        {
                            thumbnail = MediaStore.Images.Media.getBitmap(
                                    getActivity().getContentResolver(), imageUri);

//                            // Gets the width you want it to be
//                            int intendedWidth = profile_pic.getWidth();
//
//                            // Gets the downloaded image dimensions
//                            int originalWidth = thumbnail.getWidth();
//                            int originalHeight = thumbnail.getHeight();
//
//                            // Calculates the new dimensions
//                            float scale = (float) intendedWidth / originalWidth;
//                            int newHeight = (int) Math.round(originalHeight * scale);
//
//                            // Resizes mImageView. Change "FrameLayout" to whatever layout mImageView is located in.
//                            profile_pic.setLayoutParams(new FrameLayout.LayoutParams(
//                                    FrameLayout.LayoutParams.WRAP_CONTENT,
//                                    FrameLayout.LayoutParams.WRAP_CONTENT));
//                            profile_pic.getLayoutParams().width = intendedWidth;
//                            profile_pic.getLayoutParams().height = newHeight;
                            profile_pic.setImageBitmap(getCircleBitmap(thumbnail));
                            imageurl = getRealPathFromURI(imageUri);

                            Log.v(" test "," the pic path is "+imageurl);
                            DBOps.insert_profile_pic_path(getContext(),imageurl,PostValue.get_current_user_id());

                            try {

                                String database_name = DBHelper.DATABASE_NAME;
                                File sd = Environment.getExternalStorageDirectory();
                                File datas = Environment.getDataDirectory();

                                if (sd.canWrite()) {
                                    String currentDBPath = "//data//" + getActivity().getPackageName() + "//databases//"
                                            + database_name + "";
                                    String backupDBPath = "backupname.db";
                                    File currentDB = new File(datas, currentDBPath);
                                    File backupDB = new File(sd, backupDBPath);

                                    if (currentDB.exists()) {
                                        FileChannel src = new FileInputStream(currentDB).getChannel();
                                        FileChannel dst = new FileOutputStream(backupDB).getChannel();
                                        dst.transferFrom(src, 0, src.size());
                                        src.close();
                                        dst.close();
                                    }
                                }
                            } catch (Exception e) {

                            }

//                            String MY_PREFS_NAME="mysharedpreferences";
//                            SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME,getActivity().MODE_PRIVATE).edit();
//                            editor.putString("profile_pic_url",imageurl);
//                            editor.commit();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:

                boolean writeAccepted = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                if(writeAccepted)
                {
                    open_camera_intent();
                }
                else
                {
                    //do nothing
                }
                break;

        }

    }

//
//     void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode)
//        {
//
//            case PICTURE_RESULT:
//                if (requestCode == PICTURE_RESULT)
//                    if (resultCode == Activity.RESULT_OK) {
//                        try
//                        {
//                            thumbnail = MediaStore.Images.Media.getBitmap(
//                                    getContentResolver(), imageUri);
//                            my_pic.setImageBitmap(getCircleBitmap(thumbnail));
//                            imageurl = getRealPathFromURI(imageUri);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//        }
//    }

    public class ProgressBack extends AsyncTask<String, String, Bitmap>
    {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Bitmap doInBackground(String... arg0) {

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                Log.v(" test "," pic url is "+pic_url);
                InputStream input = new java.net.URL(pic_url).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result)
        {
            progress.cancel();
            profile_pic.setImageBitmap(getCircleBitmap(result));
        }
    }


    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

}