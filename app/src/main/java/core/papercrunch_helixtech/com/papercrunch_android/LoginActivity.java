package core.papercrunch_helixtech.com.papercrunch_android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import java.util.Arrays;
public class LoginActivity extends AppCompatActivity implements AccountManager.SignInCallback {

    Button loginButton;
    Button facebook_login;
    FacebookManager facebookManager;
    AccountManager accountManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_login);
        loginButton = (Button) findViewById(R.id.login_button);
        facebook_login = (Button) findViewById(R.id.login_facebook_button);

        accountManager = new AccountManager(LoginActivity.this);
        facebookManager = new FacebookManager(accountManager, this);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                Intent open_main_activity = new Intent(LoginActivity.this,Signin_or_Signup.class);
                LoginActivity.this.startActivity(open_main_activity);
            }
        });


        facebookManager.setLoginButton(findViewById(R.id.login_facebook_button), this, Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));


//        facebook_login.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        accountManager.setSignInCallback(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent open_login_options = new Intent(LoginActivity.this,Login_options.class);
        LoginActivity.this.startActivity(open_login_options);
        super.onBackPressed();
    }

    @Override
    public void sendingDataToServer() {

    }

    @Override
    public void signedInSuccessfully() {

    }

    @Override
    public void signInError(int error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == GOOGLE_SIGN_IN) {
//            googleManager.onActivityResult(requestCode, resultCode, data);
//
//        }
//        else
//        {//twitter and facebook
//            Log.v(" test "," on activity result ");
//            twitterManager.onActivityResult(requestCode, resultCode, data);
//            facebookManager.onActivityResult(requestCode, resultCode, data);
//            //loginButton.onActivityResult(requestCode, resultCode, data);
////            callbackManager.onActivityResult(requestCode, resultCode, data);
//        }
        facebookManager.onActivityResult(requestCode, resultCode, data);
    }

    public void grant_access_on_fb_login()
    {
        finish();
        String MY_PREFS_NAME="mysharedpreferences";
        SharedPreferences.Editor editor = LoginActivity.this.getSharedPreferences(MY_PREFS_NAME,LoginActivity.this.MODE_PRIVATE).edit();
        editor.putString("facebooklogin","1");
        editor.commit();
        Intent open_main_activity = new Intent(LoginActivity.this,MainActivity.class);
        LoginActivity.this.startActivity(open_main_activity);
    }
}
