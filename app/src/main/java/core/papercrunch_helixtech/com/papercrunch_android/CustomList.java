package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by helixtech-android on 25/11/16.
 */
public class CustomList extends ArrayAdapter<String> {

    Save_Paper save_paper;
//    NotificationManager mNotifyManager;
//    NotificationCompat.Builder mBuilder;
    int id=0;
    String file_name;
    String pdf_url = "https://www.papercrunch.in/examLibrary/CI3/2010/DEC/ci3-2010-dec-ci3.6.pdf";
    ProgressDialog progress;
    private final Activity context;
    private final String[] subjects;
    private final Integer[] imageId;
    private final String[] papercodes;
    private final String[] exam_dates;
    public CustomList(Activity context,
                      String[] subject,String[] papercode,String[] examdates,Integer[] imageId)
    {
        super(context, R.layout.custom_list_view, subject);
        this.context = context;
        this.subjects = subject;
        this.imageId = imageId;
        this.papercodes = papercode;
        this.exam_dates = examdates;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.custom_list_view, null, true);
        final TextView subject_title = (TextView) rowView.findViewById(R.id.subject_name);
        TextView papercode_title = (TextView) rowView.findViewById(R.id.paper_code);
        final TextView exam_date_title = (TextView) rowView.findViewById(R.id.examination_date);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        final ImageView bookmark_imageview = (ImageView) rowView.findViewById(R.id.bookmark_image);
        final ImageView saved_image = (ImageView) rowView.findViewById(R.id.save_image);

        bookmark_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookmark_imageview.setImageResource(R.drawable.bookmark_filled);
            }
        });

        saved_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // calculate the file name from subject name and exam year
                file_name = subjects[position]+"_"+(exam_dates[position].substring(exam_dates[position].indexOf(",")+2));
                String withoutspace = file_name.replaceAll("/","-");
                file_name = withoutspace;

                //NOTE: the pdf url will change depending upon the chosen paper.
                save_paper = new Save_Paper();
                id = id+1;
                Toast.makeText(context,file_name+".pdf download started",Toast.LENGTH_SHORT).show();
                save_paper.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR
                        ,file_name
                        ,pdf_url
                        ,""+id
                        ,subjects[position]
                        ,exam_dates[position]
                        ,papercodes[position]);
                saved_image.setImageResource(R.drawable.download_filled);
            }
        });



        subject_title.setText(subjects[position]);
        papercode_title.setText(papercodes[position]);
        exam_date_title.setText(exam_dates[position]);
        imageView.setImageResource(imageId[position]);
        return rowView;
    }


    public class Save_Paper extends AsyncTask<String, String, String>
    {
        NotificationManager mNotifyManager;
        NotificationCompat.Builder mBuilder;
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... arg0) {


            mNotifyManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(context);

            mBuilder.setContentTitle(arg0[0])
                    .setContentText("Download in progress")
                    .setSmallIcon(R.drawable.paper);

            String extStorageDirectory="";
            String pdf_name = arg0[0]+".pdf";
            try {
                Log.v(" test "," into progress back");

                URL url = new URL(arg0[1]);
                URLConnection conection = url.openConnection();
                conection.connect();
                conection.getContentLength();
                long total = 0;
                long contentLength = Long.parseLong(conection.getHeaderField("Content-Length"));
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                extStorageDirectory = Environment.getExternalStorageDirectory().toString();

                // Output stream to write file
                OutputStream output = new FileOutputStream(extStorageDirectory+"/" + pdf_name);

                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                while((bufferLength = input.read(buffer))>0 )
                {

                    output.write(buffer, 0, bufferLength);
                    total += bufferLength;
                    mBuilder.setProgress(100,(int) ((total * 100) / contentLength), false);
                    mNotifyManager.notify(Integer.parseInt(arg0[2]), mBuilder.build());
                    //publishProgress("" + (int) ((total * 100) / contentLength));
                }

                // update the file status here itself.
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String file_loc = extStorageDirectory+"/" + pdf_name;
            return arg0[2]+"*"+file_loc+"*"+arg0[3]+"*"+arg0[4]+"*"+arg0[5];
        }

        @Override
        protected void onPostExecute(String result)
        {
            String id = result.substring(0,result.indexOf("*"));

            result = result.substring(result.indexOf("*")+1);

            //getting the file location
            String file_location = result.substring(0,result.indexOf("*"));
            result = result.substring(result.indexOf("*")+1);

            //getting the subject name
            String sub_name = result.substring(0,result.indexOf("*"));
            result = result.substring(result.indexOf("*")+1);

            //getting the exam date
            String exam_date = result.substring(0,result.indexOf("*"));
            result = result.substring(result.indexOf("*")+1);

            //getting the subject code
            String sub_code = result;

            String MY_PREFS_NAME="mysharedpreferences";
            SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME,context.MODE_PRIVATE).edit();
            editor.putString("file",file_location);
            editor.commit();


            Intent open_pdf_render = new Intent(context,PDFRenderFragment.class);
            open_pdf_render.putExtra("file", file_location);

            PendingIntent contentIntent =
                    PendingIntent.getActivity(context, 0,open_pdf_render, 0);

            mBuilder.setContentIntent(contentIntent);
            // When the loop is finished, updates the notification
            mBuilder.setContentText("Download complete")
                    // Removes the progress bar
                    .setProgress(0,0,false);
            mNotifyManager.notify(Integer.parseInt(id), mBuilder.build());


            //query to insert the info in database
            DBOps.insert_paper_info_on_save(context,PostValue.get_current_user_id(),sub_name,sub_code,exam_date,file_location);
        }
    }
}
