package core.papercrunch_helixtech.com.papercrunch_android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper
{
	private static final int DATABASE_VERSION = 1;

	public static final String DATABASE_NAME = "Papercrunch.db";
	Context context;

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// query to create the PAPER table.
		final String CREATE_PAPER_TABLE = "CREATE TABLE "
				+ DBContract.Content.PAPER_TABLE + " ("
				+ DBContract.Content.user_id + " TEXT , "
				+ DBContract.Content.paper_subject_name + " TEXT , "
				+ DBContract.Content.paper_exam_date + " TEXT , "
				+ DBContract.Content.paper_subject_code + " TEXT,"
				+ DBContract.Content.paper_save_location + " TEXT" + ")";
		db.execSQL(CREATE_PAPER_TABLE);

		// query to create the ACADEMIC table.
		final String CREATE_ACADEMIC_TABLE = "CREATE TABLE "
				+ DBContract.Content.ACADEMIC_DETAILS + " ("
				+ DBContract.Content.user_id + " TEXT , "
				+ DBContract.Content.college_name + " TEXT , "
				+ DBContract.Content.college_branch + " TEXT , "
				+ DBContract.Content.current_semester + " TEXT,"
				+ DBContract.Content.elective_subjects + " TEXT,"
				+ DBContract.Content.backlog_subjects + " TEXT" + ")";
		db.execSQL(CREATE_ACADEMIC_TABLE);


		// query to create the PROFILE table.
		final String CREATE_PROFILE_TABLE = "CREATE TABLE "
				+ DBContract.Content.PROFILE_DETAILS + " ("
				+ DBContract.Content.user_id + " TEXT , "
				+ DBContract.Content.first_name + " TEXT , "
				+ DBContract.Content.last_name + " TEXT , "
				+ DBContract.Content.profile_pic_path + " TEXT,"
				+ DBContract.Content.email + " TEXT,"
				+ DBContract.Content.password + " TEXT" + ")";
		db.execSQL(CREATE_PROFILE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
}
