package core.papercrunch_helixtech.com.papercrunch_android;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Login_options extends AppCompatActivity {

    Button i_am_new_here,just_log_me_in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_login_options);

        i_am_new_here = (Button) findViewById(R.id.i_am_new);
        just_log_me_in = (Button) findViewById(R.id.just_log_me);

        i_am_new_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //open signup page
                finish();
                Intent open_sign_up = new Intent(Login_options.this,Signup_activity.class);
                Login_options.this.startActivity(open_sign_up);
            }
        });


        just_log_me_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //open the login activity
                finish();
                Intent signup_or_signin = new Intent(Login_options.this,LoginActivity.class);
                Login_options.this.startActivity(signup_or_signin);
            }
        });
    }
}
