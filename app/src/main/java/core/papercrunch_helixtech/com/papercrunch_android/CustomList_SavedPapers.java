package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class CustomList_SavedPapers extends ArrayAdapter<String> {

    //    NotificationManager mNotifyManager;
//    NotificationCompat.Builder mBuilder;
    int id=0;
    String file_name;
    String pdf_url = "https://www.papercrunch.in/examLibrary/CI3/2010/DEC/ci3-2010-dec-ci3.6.pdf";
    ProgressDialog progress;
    private final Activity context;
    private final String[] subjects;
    private final Integer[] imageId;
    private final String[] papercodes;
    private final String[] exam_dates;
    public CustomList_SavedPapers(Activity context,
                      String[] subject,String[] papercode,String[] examdates,Integer[] imageId)
    {
        super(context, R.layout.custom_saved_list_view, subject);
        this.context = context;
        this.subjects = subject;
        this.imageId = imageId;
        this.papercodes = papercode;
        this.exam_dates = examdates;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.custom_saved_list_view, null, true);
        final TextView subject_title = (TextView) rowView.findViewById(R.id.saved_subject);
        TextView papercode_title = (TextView) rowView.findViewById(R.id.saved_paper_code);
        final TextView exam_date_title = (TextView) rowView.findViewById(R.id.saved_examination_date);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);


        subject_title.setText(subjects[position]);
        papercode_title.setText(papercodes[position]);
        exam_date_title.setText(exam_dates[position]);
        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}