package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class CustomList_ChatWindow extends ArrayAdapter<String>
{
    private final Activity context;
    private  String[] messages;
    private String[] sender_or_receiver;


    public CustomList_ChatWindow(Activity context,
                      String[] subject,String[] person)
    {
        super(context, R.layout.custom_list_chat_window, subject);
        this.context = context;
        this.messages = subject;
        this.sender_or_receiver = person;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list_chat_window, null, true);
        final RelativeLayout layout = (RelativeLayout) rowView.findViewById(R.id.text_message_layout);
        final TextView message_title = (TextView) rowView.findViewById(R.id.text_message);

        if (sender_or_receiver[position].equals("sender")) {

            message_title.setBackgroundResource(R.drawable.rounded_background_sender);
            layout.setGravity(Gravity.RIGHT|Gravity.CENTER);
        }
        else if (sender_or_receiver[position].equals("receiver"))
        {
            message_title.setBackgroundResource(R.drawable.rounded_background_receiver);
            layout.setGravity(Gravity.LEFT|Gravity.CENTER);
        }
        message_title.setText(messages[position]);
        message_title.setPadding(10,10,10,10);
        return rowView;
    }


    public void dataChange(String[] passed_array) {
        this.messages = passed_array;
        notifyDataSetChanged();
    }
}
