package core.papercrunch_helixtech.com.papercrunch_android;

import java.util.ArrayList;

/**
 * Created by helixtech-android on 18/11/16.
 */
public class PostValue
{
    static String device_token;
    static String user_id;
    static String username;
    static String password;
    static String current_layout;

    static ArrayList<String> user_list = new ArrayList<String>();
    static ArrayList<String> user_id_list = new ArrayList<String>();
    public static void set_username(String u_name)
    {
        username = u_name;
    }


    public static String get_username()
    {
        return username;
    }

    public static void set_password(String pwd)
    {
        password=pwd;
    }

    public static String get_password()
    {
        return password;
    }

    public static void set_current_paper_layout(String current)
    {
        current_layout=current;
    }

    public static String get_current_paper_layout()
    {
        return current_layout;
    }

    public static void set_current_user_id(String uid)
    {
        user_id=uid;
    }

    public static String get_current_user_id()
    {
        return user_id;
    }

    public static void set_user_list(ArrayList<String> uid)
    {
        user_list=uid;
    }

    public static ArrayList<String> get_user_list()
    {
        return user_list;
    }

    public static void set_user_id_list(ArrayList<String> uid)
    {
        user_id_list=uid;
    }

    public static ArrayList<String> get_user_id_list()
    {
        return user_id_list;
    }

    public static void set_device_token(String uid)
    {
        device_token=uid;
    }

    public static String get_device_token()
    {
        return device_token;
    }

}
