package core.papercrunch_helixtech.com.papercrunch_android;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
public class MainActivity extends AppCompatActivity {

    TextView tabOne,tabTwo,tabThree,tabFour,tabFive;

    TextView optionTabOne,optionTabTwo,optionTabThree;

    TextView paper_optionTabOne,paper_optionTabTwo,paper_optionTabThree;

    TextView you_optionTabOne,you_optionTabTwo,you_optionTabThree;

    SearchView search;

    ImageView filter_image;

    //private Toolbar toolbar;
    private TabLayout tabLayout;

    private TabLayout option_tab_layout,paper_option_layout,you_option_layout;
    private ViewPager viewPager;
    private ViewPager viewPager_option;
    private ViewPager viewPager_paper_option;
    private ViewPager viewPager_you_option;
    private int[] tabIcons = {
            R.drawable.read_inactive,
            R.drawable.paper_inactive,
            R.drawable.chat_inactive,
            R.drawable.notify_inactive,
            R.drawable.profile_inactive
    };

    LinearLayout search_box_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_main);

        search_box_layout = (LinearLayout) findViewById(R.id.search_box_layout);
        search = (SearchView) findViewById(R.id.search_box);
        search.setFocusable(false);
        filter_image = (ImageView) findViewById(R.id.filter_icon);
        filter_image.setVisibility(View.GONE);

       // search.getBackground().setAlpha(51);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        viewPager_option = (ViewPager) findViewById(R.id.option_viewpager);
        setupViewPager_option(viewPager_option);

        viewPager_paper_option = (ViewPager) findViewById(R.id.paper_option_viewpager);
        setupViewPager_paper_option(viewPager_paper_option);

        viewPager_you_option = (ViewPager) findViewById(R.id.you_option_viewpager);
        setupViewPager_you_option(viewPager_you_option);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        option_tab_layout = (TabLayout) findViewById(R.id.option_tabs);

        paper_option_layout = (TabLayout) findViewById(R.id.paper_options_tab);
        you_option_layout = (TabLayout) findViewById(R.id.you_options_tab);



        tabLayout.setupWithViewPager(viewPager);
        option_tab_layout.setupWithViewPager(viewPager_option);
        paper_option_layout.setupWithViewPager(viewPager_paper_option);
        you_option_layout.setupWithViewPager(viewPager_you_option);

        //setting the layout for each tab
        tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFive = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        optionTabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        optionTabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        optionTabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        paper_optionTabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        paper_optionTabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        paper_optionTabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        you_optionTabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        you_optionTabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        you_optionTabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        //setting the text for each tab
        tabOne.setText(getResources().getString(R.string.read_text));
        tabTwo.setText(getResources().getString(R.string.paper_text));
        tabThree.setText(getResources().getString(R.string.chat_text));
        tabFour.setText(getResources().getString(R.string.notify_text));
        tabFive.setText(getResources().getString(R.string.you_text));

        //setting the gravity of the text to center
        tabOne.setGravity(Gravity.CENTER);
        tabTwo.setGravity(Gravity.CENTER);
        tabThree.setGravity(Gravity.CENTER);
        tabFour.setGravity(Gravity.CENTER);
        tabFive.setGravity(Gravity.CENTER);


        //for options tab
        optionTabOne.setText(getResources().getString(R.string.feed_text));
        optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
        optionTabTwo.setText(getResources().getString(R.string.bookmarks_text));
        optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
        optionTabThree.setText(getResources().getString(R.string.trending_text));
        optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));

        //setting the gravity of the text to center
        optionTabOne.setGravity(Gravity.CENTER);
        optionTabTwo.setGravity(Gravity.CENTER);
        optionTabThree.setGravity(Gravity.CENTER);


        //for papers options tab
        paper_optionTabOne.setText(getResources().getString(R.string.papers_text));
        paper_optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
        paper_optionTabTwo.setText(getResources().getString(R.string.bookmarks_text));
        paper_optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
        paper_optionTabThree.setText(getResources().getString(R.string.saved_text));
        paper_optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));

        //setting the gravity of the text to center
        paper_optionTabOne.setGravity(Gravity.CENTER);
        paper_optionTabTwo.setGravity(Gravity.CENTER);
        paper_optionTabThree.setGravity(Gravity.CENTER);

        //for you options tab
        you_optionTabOne.setText(getResources().getString(R.string.profile_text));
        you_optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
        you_optionTabTwo.setText(getResources().getString(R.string.statistics_text));
        you_optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
        you_optionTabThree.setText(getResources().getString(R.string.notification_text));
        you_optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));

        //setting the gravity of the text to center
        paper_optionTabOne.setGravity(Gravity.CENTER);
        paper_optionTabTwo.setGravity(Gravity.CENTER);
        paper_optionTabThree.setGravity(Gravity.CENTER);

        //setting the gravity of the text to center
        you_optionTabOne.setGravity(Gravity.CENTER);
        you_optionTabTwo.setGravity(Gravity.CENTER);
        you_optionTabThree.setGravity(Gravity.CENTER);

        //setting the background drawables.
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.read_active, 0, 0);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.paper_inactive, 0, 0);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat_inactive, 0, 0);
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notify_inactive, 0, 0);
        tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile_inactive, 0, 0);

        tabOne.setTextColor(getResources().getColor(R.color.active_green));

        viewPager_option.setVisibility(View.GONE);
        paper_option_layout.setVisibility(View.GONE);
        you_option_layout.setVisibility(View.GONE);

        tabLayout.getTabAt(0).setCustomView(tabOne);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
        tabLayout.getTabAt(2).setCustomView(tabThree);
        tabLayout.getTabAt(3).setCustomView(tabFour);
        tabLayout.getTabAt(4).setCustomView(tabFive);

        option_tab_layout.getTabAt(0).setCustomView(optionTabOne);
        option_tab_layout.getTabAt(1).setCustomView(optionTabTwo);
        option_tab_layout.getTabAt(2).setCustomView(optionTabThree);

        paper_option_layout.getTabAt(0).setCustomView(paper_optionTabOne);
        paper_option_layout.getTabAt(1).setCustomView(paper_optionTabTwo);
        paper_option_layout.getTabAt(2).setCustomView(paper_optionTabThree);

        you_option_layout.getTabAt(0).setCustomView(you_optionTabOne);
        you_option_layout.getTabAt(1).setCustomView(you_optionTabTwo);
        you_option_layout.getTabAt(2).setCustomView(you_optionTabThree);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager_option.setVisibility(View.GONE);
                viewPager_paper_option.setVisibility(View.GONE);
                viewPager_you_option.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
                setupTabIcons(tab.getPosition(),"tab");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        option_tab_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager_option.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.GONE);
                viewPager_paper_option.setVisibility(View.GONE);
                viewPager_you_option.setVisibility(View.GONE);
                setupTabIcons(tab.getPosition(),"options");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        paper_option_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager_option.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                viewPager_paper_option.setVisibility(View.VISIBLE);
                viewPager_you_option.setVisibility(View.GONE);
                setupTabIcons(tab.getPosition(),"paper_options");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        you_option_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager_option.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                viewPager_paper_option.setVisibility(View.GONE);
                viewPager_you_option.setVisibility(View.VISIBLE);
                setupTabIcons(tab.getPosition(),"you_options");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setupTabIcons(int position,String string)
    {
        Log.v(" test "," into set up ");
        if(string.equals("options"))
        {
            if(position == 0)
            {
                optionTabOne.setTextColor(Color.WHITE);
                optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
                optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));
                option_tab_layout.getTabAt(0).setCustomView(optionTabOne);
            }
            else if(position == 1)
            {
                optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
                optionTabTwo.setTextColor(Color.WHITE);
                optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));
                option_tab_layout.getTabAt(1).setCustomView(optionTabTwo);
            }
            else if(position == 2)
            {
                optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
                optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
                optionTabThree.setTextColor(Color.WHITE);
                option_tab_layout.getTabAt(2).setCustomView(optionTabThree);
            }
        }
        else if(string.equals("paper_options"))
        {
            if(position == 0)
            {
                paper_optionTabOne.setTextColor(Color.WHITE);
                paper_optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
                paper_optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));
                paper_option_layout.getTabAt(0).setCustomView(paper_optionTabOne);
            }
            else if(position == 1)
            {
                paper_optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
                paper_optionTabTwo.setTextColor(Color.WHITE);
                paper_optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));
                paper_option_layout.getTabAt(1).setCustomView(paper_optionTabTwo);
            }
            else if(position == 2)
            {
                paper_optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
                paper_optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
                paper_optionTabThree.setTextColor(Color.WHITE);
                paper_option_layout.getTabAt(2).setCustomView(paper_optionTabThree);
            }
        }
        else if(string.equals("you_options"))
        {
            if(position == 0)
            {
                you_optionTabOne.setTextColor(Color.WHITE);
                you_optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
                you_optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));
                you_option_layout.getTabAt(0).setCustomView(you_optionTabOne);
            }
            else if(position == 1)
            {
                you_optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
                you_optionTabTwo.setTextColor(Color.WHITE);
                you_optionTabThree.setTextColor(getResources().getColor(R.color.inactive_option_green));
                you_option_layout.getTabAt(1).setCustomView(you_optionTabTwo);
            }
            else if(position == 2)
            {
                you_optionTabOne.setTextColor(getResources().getColor(R.color.inactive_option_green));
                you_optionTabTwo.setTextColor(getResources().getColor(R.color.inactive_option_green));
                you_optionTabThree.setTextColor(Color.WHITE);
                you_option_layout.getTabAt(2).setCustomView(you_optionTabThree);
            }
        }
        else
        {
            if(position != 1)
            {
                filter_image.setVisibility(View.GONE);
            }
            else
            {
                filter_image.setVisibility(View.VISIBLE);
            }

            Log.v(" test "," position clicked is "+position);
            //now check for the position
            if(position == 0)
            {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.read_active, 0, 0);
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.paper_inactive, 0, 0);
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat_inactive, 0, 0);
                tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notify_inactive, 0, 0);
                tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile_inactive, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.active_green));
                tabTwo.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabThree.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFour.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFive.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabLayout.getTabAt(0).setCustomView(tabOne);

//                //setting the feed layout
//                TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabLayout);
//                tabhost.getTabAt(2).select();
//
                option_tab_layout.setVisibility(View.VISIBLE);
                paper_option_layout.setVisibility(View.GONE);
                you_option_layout.setVisibility(View.GONE);
                search_box_layout.setVisibility(View.VISIBLE);
                //option_tab_layout.getTabAt(0).select();
            }
            else if(position == 1)
            {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.read_inactive, 0, 0);
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.paper_active, 0, 0);
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat_inactive, 0, 0);
                tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notify_inactive, 0, 0);
                tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile_inactive, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabTwo.setTextColor(getResources().getColor(R.color.active_green));
                tabThree.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFour.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFive.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabLayout.getTabAt(1).setCustomView(tabTwo);

                //hiding the options tab
                option_tab_layout.setVisibility(View.GONE);
                you_option_layout.setVisibility(View.GONE);
                paper_option_layout.setVisibility(View.VISIBLE);
                search_box_layout.setVisibility(View.VISIBLE);
                //search_box_layout.setVisibility(View.GONE);
                //paper_option_layout.getTabAt(0).select();
            }
            else if(position == 2)
            {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.read_inactive, 0, 0);
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.paper_inactive, 0, 0);
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat_active, 0, 0);
                tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notify_inactive, 0, 0);
                tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile_inactive, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabTwo.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabThree.setTextColor(getResources().getColor(R.color.active_green));
                tabFour.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFive.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabLayout.getTabAt(2).setCustomView(tabThree);

                option_tab_layout.setVisibility(View.GONE);
                paper_option_layout.setVisibility(View.GONE);
                search_box_layout.setVisibility(View.GONE);
                you_option_layout.setVisibility(View.GONE);
               // you_option_layout.setVisibility(View.GONE);
            }
            else if(position == 3)
            {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.read_inactive, 0, 0);
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.paper_inactive, 0, 0);
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat_inactive, 0, 0);
                tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notify_active, 0, 0);
                tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile_inactive, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabTwo.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabThree.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFour.setTextColor(getResources().getColor(R.color.active_green));
                tabFive.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabLayout.getTabAt(3).setCustomView(tabFour);

                option_tab_layout.setVisibility(View.GONE);
                paper_option_layout.setVisibility(View.GONE);
                search_box_layout.setVisibility(View.VISIBLE);
                //you_option_layout.setVisibility(View.GONE);
            }
            else if(position == 4)
            {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.read_inactive, 0, 0);
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.paper_inactive, 0, 0);
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat_inactive, 0, 0);
                tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notify_inactive, 0, 0);
                tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile_active, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabTwo.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabThree.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFour.setTextColor(getResources().getColor(R.color.inactive_grey));
                tabFive.setTextColor(getResources().getColor(R.color.active_green));
                tabLayout.getTabAt(4).setCustomView(tabFive);

                //hiding the options tab
                option_tab_layout.setVisibility(View.GONE);
                you_option_layout.setVisibility(View.VISIBLE);
                paper_option_layout.setVisibility(View.GONE);
                search_box_layout.setVisibility(View.VISIBLE);
                //you_option_layout.getTabAt(0).select();
            }
        }

    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Read_Fragment(),getResources().getString(R.string.read_text));
        adapter.addFrag(new Papers_option_Fragment(),getResources().getString(R.string.paper_text));
        adapter.addFrag(new Chat_Fragment(),getResources().getString(R.string.chat_text));
        adapter.addFrag(new Notify_Fragment(),getResources().getString(R.string.notify_text));
        adapter.addFrag(new Profile_Fragment(),getResources().getString(R.string.you_text));
        viewPager.setAdapter(adapter);
    }

    private void setupViewPager_option(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Feed_Fragment(),getResources().getString(R.string.feed_text));
        adapter.addFrag(new Bookmarks_Fragment(),getResources().getString(R.string.bookmarks_text));
        adapter.addFrag(new Trending_Fragment(),getResources().getString(R.string.trending_text));
        viewPager.setAdapter(adapter);
    }

    //setting up the paper option viewpager
    private void setupViewPager_paper_option(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Papers_option_Fragment(),getResources().getString(R.string.papers_text));
        adapter.addFrag(new Papers_bookmarks_option_Fragment(),getResources().getString(R.string.bookmarks_text));
        adapter.addFrag(new Saved_Fragment(),getResources().getString(R.string.saved_text));
        viewPager.setAdapter(adapter);
    }

    //setting up the you option viewpager
    private void setupViewPager_you_option(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Profile_Fragment(),getResources().getString(R.string.profile_text));
        adapter.addFrag(new Statistics_Fragment(),getResources().getString(R.string.statistics_text));
        adapter.addFrag(new Notification_Fragment(),getResources().getString(R.string.notification_text));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
//
//
//    @Override
//    public void onBackPressed() {
//
//        if (PostValue.get_current_paper_layout().equals("papers"))
//        {
//            super.onBackPressed();
//        }
//        else
//        {
//            //do nothing
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//
//        if (PostValue.get_current_paper_layout().equals("papers"))
//        {
//            super.onDestroy();
//        }
//        else
//        {
//            //do nothing
//        }
//
//    }


    @Override
    protected void onResume() {
        super.onResume();
        search.setFocusable(false);
    }
}