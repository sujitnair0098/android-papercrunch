package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class ChatWindowActivity extends AppCompatActivity {



    //ArrayList of messages to store the thread messages
    private ArrayList<String> messages_arraylist = new ArrayList<String>();

    //ArrayList to store the person.
    private ArrayList<String> person_arraylist = new ArrayList<String>();

    String[] messages_array;
    String[] person_array;
    CustomList_ChatWindow adapter;

    //Broadcast receiver to receive broadcasts
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    ProgressDialog progress;
    //String[] messages;
    ListView messages_list;
    EditText message_text;
    TextView send;
    TextView receiver_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_chat_window);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        Intent get_intent = getIntent();
        final String receiver_id = get_intent.getStringExtra("RECEIVERID");
        final String receivername = get_intent.getStringExtra("RECEIVERNAME");
        message_text = (EditText) findViewById(R.id.text_message);
        send = (TextView) findViewById(R.id.send_message);
        messages_list = (ListView) findViewById(R.id.messages_listview);
        receiver_name = (TextView) findViewById(R.id.chat_window_receiver_name);
        receiver_name.setText(receivername);

        //message_text.setFocusable(false);

        String get_messages_url ="http://httest.in/papercrunchws/RUC_GetUserMessages/loggedin_user_id/" +
                PostValue.get_current_user_id()+"/other_user_id/"+receiver_id;

        progress = ProgressDialog.show(ChatWindowActivity.this, "Loading",
                "", true);

       // get_messages_url = "http://httest.in/papercrunchws/RUC_GetUserMessages/loggedin_user_id/230/other_user_id/247";
        //making a webservice call to get all the messages and display in the chat window.
        get_messages(get_messages_url);



        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Constants.REGISTRATION_COMPLETE))
                {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_GLOBAL);

                   // displayFirebaseRegId();

                }
                else if (intent.getAction().equals(Constants.PUSH_NOTIFICATION))
                {
                    // new push notification is received
                    String name = intent.getStringExtra("name");
                    String message = intent.getStringExtra("message");
                    String message_id=intent.getStringExtra("message_id");
                    String receiver_id=intent.getStringExtra("receiver_id");
                    String sender_id=intent.getStringExtra("sender_id");
                    String sent_date=intent.getStringExtra("sent_date");

                   // Toast.makeText(getApplicationContext(), "Push notification: " + message + message_id ,Toast.LENGTH_LONG).show();


                    //txtMessage.setText(message);

                    String messages_url="http://httest.in/papercrunchws/RUC_SingleMessageDetails/message_id/"+message_id;
                    get_messages(messages_url);



                }
            }
        };

        displayFirebaseRegId();



        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="http://httest.in/papercrunchws/RUC_SendMessage/sender_id/" +
                        PostValue.get_current_user_id()+"/receiver_id/" +
                        receiver_id+"/message/"
                        +message_text.getText().toString();


//                url="http://httest.in/papercrunchws/RUC_SendMessage/sender_id/" +
//                        "230"+"/receiver_id/" +
//                        "247"+"/message/"
//                        +message_text.getText().toString();

                web_service_call(url);
            }
        });

        displayFirebaseRegId();

    }
//    String message_id;
//    String messages_url="http://httest.in/papercrunchws/RUC_SingleMessageDetails/message_id/"+message_id;
    @Override
    protected void onStart() {


        //get_messages(messages_url);

        super.onStart();
    }

    //This method will return current timestamp
    public static String getTimeStamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e("test", "Firebase reg id: " + regId);

//        if (!TextUtils.isEmpty(regId))
//            txtRegId.setText("Firebase Reg Id: " + regId);
//        else
//            txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    @Override
    protected void onResume() {
        super.onResume();





        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.PUSH_NOTIFICATION));

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(Constants.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }



    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    private void web_service_call(String url)
    {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("Response", s);
                try {
                    Log.v(" test "," into try ");
                    JSONObject object = new JSONObject(s);
                    Log.v(" test "," response is "+object.getInt("response"));
                    if (object.getInt("response") == 1)
                    {
                        messages_arraylist.add(message_text.getText().toString());
                        person_arraylist.add("sender");
                        /*if(object.getString("sender_id").equals(""+PostValue.get_current_user_id()))
                        {
                            person_arraylist.add("sender");
                        }
                        else
                        {
                            person_arraylist.add("receiver");
                        }
*/
                        messages_array = new String[messages_arraylist.size()];
                        person_array = new String[person_arraylist.size()];
                        for(int i=0;i<messages_array.length;i++)
                        {
                            messages_array[i]=messages_arraylist.get(i);
                            person_array[i] = person_arraylist.get(i);
                        }
                        adapter = new CustomList_ChatWindow(ChatWindowActivity.this,messages_array,person_array);
                        messages_list.setAdapter(adapter);
                        messages_list.setSelection(messages_list.getCount()-1);
//                        adapter.dataChange(messages_array);

                        adapter.notifyDataSetChanged();
//                        messages_list.setAdapter(adapter);
                        message_text.setText("");


//                        JSONArray jar = object.getJSONArray("data");
//                        //Toast.makeText(getActivity(),"No of users are "+jar.length(),Toast.LENGTH_SHORT).show();
//                        values = new String[jar.length()];
//                        user_ids = new String[jar.length()];
//                        for (int i=0;i<jar.length();i++)
//                        {
//                            JSONObject rec = jar.getJSONObject(i);
//                            values[i] = rec.getString("firstName")+" "+rec.getString("lastName");
//                            user_ids[i] = rec.getString("user_id");
//                        }
//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                                android.R.layout.simple_list_item_1, android.R.id.text1, values);
//
//
//                        // Assign adapter to ListView
//                        user_listview.setAdapter(adapter);
                    }
                    else
                    {
                        //Toast.makeText(ChatWindowActivity.this,"Failed",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    Log.v(" test "," into catch block ");
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)
            {
                Log.v(" test "," no internet connection ");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;

            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    //Processing message to add on the thread

    private void get_messages(String url)
    {
        Log.v(" test "," the url is "+url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("Response", s);
                try {
                    Log.v(" test "," into try ");
                    JSONObject object = new JSONObject(s);
                    Log.v(" test "," response is "+object.getInt("response"));
                    if (object.getInt("response") == 1)
                    {
                        Log.v(" test "," successful ");
                        JSONArray jar = object.getJSONArray("data");

                        for (int i=0;i<jar.length();i++)
                        {
                            JSONObject rec = jar.getJSONObject(i);
                            messages_arraylist.add(rec.getString("message"));
                            if(rec.getString("sender_id").equals(""+PostValue.get_current_user_id()))
                            {
                                person_arraylist.add("sender");
                            }
                            else
                            {
                                person_arraylist.add("receiver");
                            }
                          //  messages[i]=rec.getString("message");
                        }

                        messages_array = new String[messages_arraylist.size()];
                        person_array = new String[person_arraylist.size()];
                        for(int i=0;i<messages_array.length;i++)
                        {
                            messages_array[i]=messages_arraylist.get(i);
                            person_array[i] = person_arraylist.get(i);
                        }
                        adapter = new
                                CustomList_ChatWindow(ChatWindowActivity.this,messages_array,person_array);
                        messages_list.setAdapter(adapter);
                        messages_list.setSelection(messages_list.getCount()-1);
                        progress.cancel();
                    }
                    else
                    {
                        progress.cancel();
//                        Toast.makeText(ChatWindowActivity.this,"Failed",Toast.LENGTH_SHORT).show();
//                        finish();
                    }
                } catch (JSONException e)
                {
                    Log.v(" test "," into catch block ");
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)
            {
                Log.v(" test "," no internet connection ");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;

            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

}
