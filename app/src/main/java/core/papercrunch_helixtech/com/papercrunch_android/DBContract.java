//this class is the class which defines the names of all the columns for the database tables, each stored seperately in
//a string variable.
package core.papercrunch_helixtech.com.papercrunch_android;

public class DBContract {

	public static final class Content {

		public static String PAPER_TABLE = "Papers_Details";

		public static String paper_save_location = "paperSaveLocation";
        public static String paper_subject_name = "paperSubjectName";
        public static String paper_exam_date = "paperExamDate";
        public static String paper_subject_code = "paperExamCode";


		public static String ACADEMIC_DETAILS = "Academic_Details";
		public static String college_name = "collegeName";
		public static String college_branch = "collegeBranch";
		public static String current_semester = "currentSemester";
		public static String elective_subjects = "electiveSubjects";
		public static String backlog_subjects = "backlogSubjects";


		public static String PROFILE_DETAILS = "Profile_Details";
		public static String first_name = "firstName";
		public static String last_name = "lastName";
		public static String email = "emailID";
		public static String password = "electiveSubjects";

		//common columns
		public static String user_id="userID";

		//common columns
		public static String profile_pic_path="profilePic";
	}

}
