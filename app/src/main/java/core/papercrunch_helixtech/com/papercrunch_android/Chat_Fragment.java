package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by helixtech-android on 12/11/16.
 */
public class Chat_Fragment extends Fragment {

    String[] updated_user_array;
    String[] updated_user_ids;
    ListView user_listview;
    String[] values;
    String[] user_ids;
    Integer[] imageId;
    String[] last_message;
    String[] last_timing;
    EditText search_user;
    CustomList_Chat adapter;
    ArrayList<String> updated_user_list=new ArrayList<String>();
    ArrayList<String> updated_user_ids_list = new ArrayList<String>();

    public Chat_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment,
                container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        progress = ProgressDialog.show(getActivity(), "Refreshing list",
//                "", true);

        search_user = (EditText) view.findViewById(R.id.search_box);
        user_listview = (ListView) view.findViewById(R.id.user_list);
        values = new String[PostValue.get_user_list().size()];
        last_message = new String[values.length];
        last_timing = new String[values.length];
        user_ids = new String[values.length];
        imageId = new Integer[values.length];
        for(int i=0;i<values.length;i++)
        {
            values[i] = PostValue.get_user_list().get(i);
            user_ids[i] = PostValue.get_user_id_list().get(i);
            last_message[i]="How are You?";
            last_timing[i]="14:56";
            imageId[i] = R.drawable.avatar;
        }

//        //Creating the instance of ArrayAdapter containing list of language names
//        ArrayAdapter<String> adapter_second = new ArrayAdapter<String>
//                (getActivity(),android.R.layout.select_dialog_item,values);

//        CustomList_Chat adapter_second = new
//                CustomList_Chat(getActivity(), values,last_message,last_timing, imageId);


         adapter = new
                CustomList_Chat(getActivity(), values,last_message,last_timing, imageId);

//        // Adding items to listview
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1
//                , R.id.product_name, values);
       // lv.setAdapter(adapter);

//        search_user.setThreshold(1);//will start working from first character
//        search_user.setAdapter(adapter_second);//setting the adapter data into the AutoCompleteTextView

        // Assign adapter to ListView
        user_listview.setAdapter(adapter);

//        String MY_PREFS_NAME="mysharedpreferences";
//        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);
//        String user_id = prefs.getString("userid",null);
//
//        String url = "http://httest.in/papercrunchws/RUC_GetLIstOfUsers/" +
//                "user_id/"+user_id+"/" +
//                "pageno/0";

//        web_service_call(url);


        search_user.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                updated_user_list = new ArrayList<String>();
                updated_user_ids_list = new ArrayList<String>();
                // When user changed the Text
                for(int i=0;i<values.length;i++)
                {
                    if(values[i].toLowerCase().contains(cs.toString().toLowerCase()))
                    {
//                        Log.v(" test "," the length is "+values.length);
//                        Log.v(" test "," values[i] being checked is "+values[i]);
//                        Log.v(" test "," char sequence is "+cs);
//
//                        Log.v(" test "," match found "+values[i]);
                        updated_user_list.add(values[i]);
                        updated_user_ids_list.add(user_ids[i]);
                    }
                }

                updated_user_array=new String[updated_user_list.size()];
                updated_user_ids = new String[updated_user_ids_list.size()];

                for(int i=0;i<updated_user_array.length;i++)
                {
                    updated_user_array[i]=updated_user_list.get(i);
                    updated_user_ids[i]=updated_user_ids_list.get(i);
                }

                adapter = new
                        CustomList_Chat(getActivity(), updated_user_array,last_message,last_timing,imageId);

                // Assign adapter to ListView
                user_listview.setAdapter(adapter);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


        user_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                // ListView Clicked item index
                int itemPosition     = i;

                // ListView Clicked item value
                String  itemValue = (String) user_listview.getItemAtPosition(i);
                String item_id;
                String item_name;

                if(search_user.getText().toString().length() != 0)
                {
                    item_id = updated_user_ids[i];
                    item_name = updated_user_array[i];
                }
                else
                {
                    item_id = user_ids[i];
                    item_name = (String) user_listview.getItemAtPosition(i);
                }




               // http://httest.in/papercrunchws/RUC_SendMessage/sender_id/230/receiver_id/247/message/test

//                // Show Alert
//                Toast.makeText(getActivity(),
//                        "Position :"+itemPosition+"  ListItem and id: " +itemValue+" and "+item_id , Toast.LENGTH_LONG)
//                        .show();


                Intent open_chat_window = new Intent(getActivity(),ChatWindowActivity.class);
                open_chat_window.putExtra("RECEIVERID",item_id);
                open_chat_window.putExtra("RECEIVERNAME",item_name);
                getActivity().startActivity(open_chat_window);


            }
        });

        user_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                return true;
            }
        });
        return view;
    }

//    private void web_service_call(String url)
//    {
//
//        Log.v(" test "," web service called ");
//        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
//        {
//            @Override
//            public void onResponse(String s)
//            {
//                Log.e("Response", s);
//                try {
//                    Log.v(" test "," into try ");
//                    JSONObject object = new JSONObject(s);
//                    Log.v(" test "," response is "+object.getInt("response"));
//                    if (object.getInt("response") == 1)
//                    {
//                        Log.v(" test "," successful ");
////                        JSONArray jar = object.getJSONArray("data");
////                        //Toast.makeText(getActivity(),"No of users are "+jar.length(),Toast.LENGTH_SHORT).show();
////                        values = new String[jar.length()];
////                        user_ids = new String[jar.length()];
////                        for (int i=0;i<jar.length();i++)
////                        {
////                            JSONObject rec = jar.getJSONObject(i);
////                            values[i] = rec.getString("firstName")+" "+rec.getString("lastName");
////                            user_ids[i] = rec.getString("user_id");
////                        }
////                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
////                                android.R.layout.simple_list_item_1, android.R.id.text1, values);
////
////
////                        // Assign adapter to ListView
////                        user_listview.setAdapter(adapter);
//                    }
//                    else
//                    {
//                        Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e)
//                {
//                    Log.v(" test "," into catch block ");
//                    e.printStackTrace();
//
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError)
//            {
//                Log.v(" test "," no internet connection ");
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError
//            {
//                return null;
//
//            }
//        };
//        VolleySingleton.getInstance().getRequestQueue().add(request);
//    }


//    public class ProgressBack extends AsyncTask<String, String, Bitmap>
//    {
//
//        @Override
//        protected void onPreExecute() {
//        }
//
//        @Override
//        protected Bitmap doInBackground(String... arg0) {
//
//            String MY_PREFS_NAME="mysharedpreferences";
//            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);
//            String user_id = prefs.getString("userid",null);
//
//            //create a for loop in which the url will be called
//            String url = "http://httest.in/papercrunchws/RUC_GetLIstOfUsers/" +
//                    "user_id/"+user_id+"/" +
//                    "pageno/0";
//            web_service_call(url);
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap result)
//        {
//            progress.cancel();
//            //profile_pic.setImageBitmap(getCircleBitmap(result));
//        }
//    }

}