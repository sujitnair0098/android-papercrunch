package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Helix Admin on 19-12-2016.
 */

public class MyFcmListenerService extends FirebaseMessagingService {

    private static final String TAG = "PAPER CRUNCH";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        //Log.d(TAG, "Notification Message Body: " + remoteMessage.getData());
        Log.v(" test "," the message is "+remoteMessage.getData());
        sendNotification(remoteMessage.getData().get("data"));
    }
    private void sendNotification(String messageBody) {
        try {
            JSONObject jsonObject = new JSONObject(messageBody);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(jsonObject.getString("title"))
                    .setContentText(jsonObject.getString("message"))
                    .setContentText(jsonObject.getString("message_id"))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);

            PendingIntent pendingIntent = null;
            if (jsonObject.has("type")) {
                if (jsonObject.getInt("type") == 1) {
                    AccountManager accountManager = new AccountManager(this);
                    if(accountManager.isSignedIn()){
                        Intent intent = new Intent(this, ChatWindowActivity.class);
                        intent.putExtra("offer_id", jsonObject.getInt("offer_id"));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                        notificationBuilder.setContentIntent(pendingIntent);
                    }

                } else if (jsonObject.getInt("type") == 2) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + jsonObject.getString("package_name")));
                    notificationBuilder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0));
                }
            }
           /* NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());*/


        //Creating a broadcast intent
        Intent pushNotification = new Intent(Constants.PUSH_NOTIFICATION);
            pushNotification.putExtra("message",jsonObject.getString("message"));
            pushNotification.putExtra("message_id",jsonObject.getString("message_id"));
        //Adding notification data to the intent

        //We will create this class to handle notifications
        NotificationHandler notificationHandler = new NotificationHandler(getApplicationContext());
        //If the app is in foreground
        if (!NotificationHandler.isAppIsInBackground(getApplicationContext())) {
            //Sending a broadcast to the chatroom to add the new message
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
        } else {
            //If app is in foreground displaying push notification
            notificationHandler.showNotificationMessage(jsonObject.getString("title"),jsonObject.getString("message"));
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}