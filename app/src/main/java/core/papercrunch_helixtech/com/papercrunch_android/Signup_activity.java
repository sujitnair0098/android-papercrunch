package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Signup_activity extends AppCompatActivity {

    int profile_pic_clicked =0;
    Bitmap thumbnail;
    private final int PICTURE_RESULT = 20;
    private Uri imageUri;
    String imageurl;

    TextView main_text;
    ProgressDialog progress;
    EditText f_name,l_name,e_id,p_text,cp_text;
    Button signup,add_profile_pic_button;
    ImageView right_corner_image,profile_image,main_image;
    String token = SharedPrefManager.getInstance(this).getDeviceToken();

  //  View view1,view2,view3,view4,view5;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        super.onCreate(savedInstanceState);

        profile_pic_clicked=0;

        setContentView(R.layout.activity_signup_activity);
        f_name = (EditText) findViewById(R.id.firstname_edittext);
        l_name = (EditText) findViewById(R.id.lastname_edittext);
        e_id = (EditText) findViewById(R.id.emailid_edittext);
        p_text = (EditText) findViewById(R.id.password_text);
        cp_text = (EditText) findViewById(R.id.confirm_password_text);
        signup = (Button) findViewById(R.id.sign_up_button);
        add_profile_pic_button = (Button) findViewById(R.id.add_profile_picture);
        right_corner_image = (ImageView) findViewById(R.id.right_corner_icon);
        profile_image = (ImageView) findViewById(R.id.profile_picture_display);
        main_text = (TextView) findViewById(R.id.papercrunch_main_text);
        main_image = (ImageView) findViewById(R.id.papercrunch_main_image);


        add_profile_pic_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //requesting permission on runtime to handle the android marshmallow version.
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1) {
                    String[] perms = {"android.permission.CAMERA"};
                    int permsRequestCode = 200;
                    requestPermissions(perms, permsRequestCode);
                }
                else
                {
                    open_camera_intent();
                }
            }
        });



//        view1 = findViewById(R.id.first_name_view);
//        view2 = findViewById(R.id.last_name_view);
//        view3 = findViewById(R.id.email_view);
//        view4 = findViewById(R.id.password_view);
//        view5 = findViewById(R.id.confirm_password_view);

        f_name.setHintTextColor(getResources().getColor(R.color.signup_label_color));
        l_name.setHintTextColor(getResources().getColor(R.color.signup_label_color));
        e_id.setHintTextColor(getResources().getColor(R.color.signup_label_color));
        p_text.setHintTextColor(getResources().getColor(R.color.signup_label_color));
        cp_text.setHintTextColor(getResources().getColor(R.color.signup_label_color));


        f_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                f_name.setText("");

            }
        });

        l_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l_name.setText("");
            }
        });

        e_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                e_id.setText("");
            }
        });

        p_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p_text.setText("");
            }
        });

        cp_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cp_text.setText("");
            }
        });


        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call the web service and based on the response, give access to the main activity.


                if (p_text.getText().toString().equals(cp_text.getText().toString()))
                {

//                    if(profile_pic_clicked != 1)
//                    {
//                        String MY_PREFS_NAME="mysharedpreferences";
//                        SharedPreferences.Editor editor = Signup_activity.this.getSharedPreferences(MY_PREFS_NAME,Signup_activity.this.MODE_PRIVATE).edit();
//                        editor.putString("profile_pic_url",null);
//                        editor.commit();
//                    }

//                    String deviceId = Settings.Secure.getString(Signup_activity.this.getContentResolver(),
//                            Settings.Secure.ANDROID_ID);

                    //encrypt the password before passing through URL.

                    String encrypted_password="";
                    try {
                        encrypted_password = EncryptionClass.MD5(p_text.getText().toString());
                    }
                    catch (Exception e)
                    {

                    }

                    Log.v(" test "," the encrypted password is "+encrypted_password);


//                    String url="http://httest.in/papercrunchws/RUC_UserSignup/" +
//                            "firstName/praba/" +
//                            "lastName/haran/" +
//                            "emailId/praba@helixtech.co/" +
//                            "password/12345/" +
//                            "deviceToken/bhhdevfe53435/" +
//                            "deviceType/1";

                    String MY_PREFS_NAME="mysharedpreferences";
                    SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                    String device_id = prefs.getString("devicetoken",null);

                    String url =
                            "http://httest.in/papercrunchws/RUC_UserSignup/" +
                                    "firstName/" + f_name.getText().toString() + "/" +
                                    "lastName/" + l_name.getText().toString() + "/" +
                                    "emailId/" + e_id.getText().toString() + "/" +
                                    "password/"+encrypted_password+"/" +
                                    "deviceToken/"+token+"/"+
                                    "deviceType/2";

                    Intent open_signup_second_screen = new Intent(Signup_activity.this,SignUp_SecondScreen.class);
                    open_signup_second_screen.putExtra("URL",url);
                    open_signup_second_screen.putExtra("EMAIL",e_id.getText().toString());
                    open_signup_second_screen.putExtra("PASSWORD",encrypted_password);
                    open_signup_second_screen.putExtra("FIRSTNAME",f_name.getText().toString());
                    open_signup_second_screen.putExtra("LASTNAME",l_name.getText().toString());
                    open_signup_second_screen.putExtra("PICCLICKED",""+profile_pic_clicked);
                    open_signup_second_screen.putExtra("PICURL",""+imageurl);
                    open_signup_second_screen.putExtra("deviceToken",""+token);
                    open_signup_second_screen.putExtra("deviceType",""+2);
                    Signup_activity.this.startActivity(open_signup_second_screen);

//                    progress = ProgressDialog.show(Signup_activity.this, "Please Wait",
//                            "", true);



                    //web_service_call(url);

                } else {
                    cp_text.setText("");
                    cp_text.setError(" incorrect password ");

                }

            }
        });


    }


    public void open_camera_intent()
    {


        //display the right corner image
        right_corner_image.setVisibility(View.VISIBLE);

        //hide the papercrunch image and the papercrunch text
        main_image.setVisibility(View.GONE);
        main_text.setVisibility(View.GONE);
        profile_image.setVisibility(View.VISIBLE);


        //open the camera intent
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = Signup_activity.this.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PICTURE_RESULT);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:

                boolean writeAccepted = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                if(writeAccepted)
                {
                    open_camera_intent();
                }
                else
                {
                    //do nothing
                }


                break;

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {

            case PICTURE_RESULT:
                if (requestCode == PICTURE_RESULT) {

                    Log.v(" test "," save here ");
                    if (resultCode == Activity.RESULT_OK) {
                        try
                        {
                            add_profile_pic_button.setText(getResources().getString(R.string.change_profile_pic_text));

                            thumbnail = MediaStore.Images.Media.getBitmap(
                                    Signup_activity.this.getContentResolver(), imageUri);
                            profile_image.setImageBitmap(getCircleBitmap(thumbnail));
                            imageurl = getRealPathFromURI(imageUri);

                            String MY_PREFS_NAME="mysharedpreferences";
                            SharedPreferences.Editor editor = Signup_activity.this.getSharedPreferences(MY_PREFS_NAME,Signup_activity.this.MODE_PRIVATE).edit();
                            editor.putString("profile_pic_url",imageurl);
                            editor.commit();

                            profile_pic_clicked=1;
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = Signup_activity.this.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent open_login_options = new Intent(Signup_activity.this,Login_options.class);
        Signup_activity.this.startActivity(open_login_options);

        super.onBackPressed();
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {

//        Paint paint = new Paint();
//        paint.setColor(Color.WHITE);
//        paint.setStrokeWidth(3);
//        canvas.drawRect(0, 0, 200, 200, paint);//draw your bg
//        canvas.drawBitmap(bitmap, 20, 20, paint);//draw your image on bg


        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        paint.setStrokeWidth(3);
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }
}
