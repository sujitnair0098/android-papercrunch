package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by helixtech-android on 12/11/16.
 */
public class Papers_option_Fragment extends Fragment {

//    //FrameLayout pdf_frame_layout;
//    ViewPager viewpager;
    String file_location_to_be_deleted="";
    ProgressDialog progress;
    String pdf_url;
    ListView list;
    GridView gridview;
    ImageAdapter image_adapter;
//    Button paper_button,bookmark_button,save_button;
//    FrameLayout fragment;

    String[] subjects = {
            "Communication Skills",
            "Logic Design",
            "Automata Language Computation",
            "Madf",
            "Database Management System",
            "Object Oriented Programming and Design",
            "Information Technology",
            "Basic Mechanical Engineering",
            "Digital Signal Processing",
            "Discrete Mathematical Structures",
            "Applied Science"
    } ;

    String[] paper_codes = {
            "FE-1-2(RC)",
            "SE-3-4(RC)",
            "TE-5-6(RC)",
            "BE-7-8(RC)",
            "FE-2-3(RC)",
            "SE-4-5(RC)",
            "TE-6-7(RC)",
            "BE-8-8(RC)",
            "FE-2-5(RC)",
            "SE-4-1(RC)",
            "BE-2-4(RC)"
    } ;

    String[] exam_date = {
            "Examination, May/June 2013",
            "Examination, Nov/Dec 2015",
            "Examination, May/June 2016",
            "Examination, Nov/Dec 2011",
            "Examination, May/June 2012",
            "Examination, Nov/Dec 2008",
            "Examination, May/June 2010",
            "Examination, Nov/Dec 2011",
            "Examination, May/June 2013",
            "Examination, Nov/Dec 2014",
            "Examination, May/June 2015"

    } ;

    Integer[] imageId;
    public Papers_option_Fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.paper_fragment,
                container, false);

//        fragment = (FrameLayout) view.findViewById(R.id.fragment_place);
//        fragment.setVisibility(View.GONE);
//        paper_button = (Button) view.findViewById(R.id.paper_button);
//        bookmark_button = (Button) view.findViewById(R.id.bookmarks_button);
//        save_button = (Button) view.findViewById(R.id.saved_button);
//        viewpager = (ViewPager) view.findViewById(R.id.viewpager);
//        viewpager.setVisibility(View.GONE);
//
//        setupViewPager_paper_option(viewpager);

        //requesting permission on runtime to handle the android marshmallow version.
        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1) {
            String[] perms = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
            int permsRequestCode = 200;
            requestPermissions(perms, permsRequestCode);
        }

//        paper_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                selectFrag(view);
//            }
//        });
//
//        bookmark_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectFrag(view);
//            }
//        });
//
//        save_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectFrag(view);
//            }
//        });

        pdf_url = "https://www.papercrunch.in/examLibrary/CI3/2010/DEC/ci3-2010-dec-ci3.6.pdf";


        imageId = new Integer[subjects.length];
        for(int i=0;i<subjects.length;i++)
        {
            imageId[i]=R.drawable.paper_pdf_image;
        }

        CustomList adapter = new
                CustomList(getActivity(), subjects,paper_codes,exam_date, imageId);
        list=(ListView)view.findViewById(R.id.user_list);
        list.setVisibility(View.GONE);
        list.setAdapter(adapter);



        gridview = (GridView) view.findViewById(R.id.gridview);
        image_adapter = new ImageAdapter(getActivity(),subjects,Papers_option_Fragment.this);
        gridview.setAdapter(image_adapter);

//
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
//            {
//                gridview.setVisibility(View.GONE);
//                fragment.setVisibility(View.VISIBLE);
//            }
//        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                progress = ProgressDialog.show(getActivity(), "Loading",
                        "", true);
                new ProgressBack().execute();
            }
        });

//        pdf_frame_layout = (FrameLayout) view.findViewById(R.id.container);
//        pdf_frame_layout.setVisibility(View.GONE);

        return view;
    }


//    public void selectFrag(View view)
//    {
//        Fragment fr=null;
//        if(view == view.findViewById(R.id.paper_button))
//        {
//            fr = new Papers_option_Fragment();
//        }
//        else if(view == view.findViewById(R.id.bookmarks_button))
//        {
//            fr = new Bookmarks_Fragment();
//        }
//        else if(view == view.findViewById(R.id.saved_button))
//        {
//            fr = new Saved_Fragment();
//        }
//        FragmentManager fm = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_place, fr);
//        fragmentTransaction.commit();
//    }


//    //setting up the paper option viewpager
//    private void setupViewPager_paper_option(ViewPager viewPager) {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFrag(new Papers_option_Fragment(),getResources().getString(R.string.papers_text));
//        adapter.addFrag(new Papers_bookmarks_option_Fragment(),getResources().getString(R.string.bookmarks_text));
//        adapter.addFrag(new Saved_Fragment(),getResources().getString(R.string.saved_text));
//        viewPager.setAdapter(adapter);
//    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:

                boolean writeAccepted = grantResults[0]== PackageManager.PERMISSION_GRANTED;

                break;

        }

    }


    public class ProgressBack extends AsyncTask<String, String, String>
    {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... arg0) {

            String extStorageDirectory="";
            String pdf_name = "mypdf" + ".pdf";
            try {
                Log.v(" test "," into progress back");

                URL url = new URL(pdf_url);
                URLConnection conection = url.openConnection();
                conection.connect();
                conection.getContentLength();
                long total = 0;
                long contentLength = Long.parseLong(conection.getHeaderField("Content-Length"));
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                extStorageDirectory = Environment.getExternalStorageDirectory().toString();

                // Output stream to write file
                OutputStream output = new FileOutputStream(extStorageDirectory+"/" + pdf_name);

                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                while((bufferLength = input.read(buffer))>0 ){
                    Log.v(" test "," into while ");
                    output.write(buffer, 0, bufferLength);
                }

                // update the file status here itself.
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return extStorageDirectory+"/" + pdf_name;
        }

        @Override
        protected void onPostExecute(String result)
        {
            progress.cancel();
            // Toast.makeText(getActivity(),"Download complete",Toast.LENGTH_LONG).show();
            // filePath the name and directory where the byte array was saved.
            File pdfFile = new File(result);
            file_location_to_be_deleted=result;
            if (pdfFile.exists()) {

                if (Build.VERSION.SDK_INT >= 21)
                {
                    ParcelFileDescriptor mFileDescriptor=null;

                    try
                    {
//                        Log.v(" test "," into the try block ");
                        Intent open_pdf_renderer = new Intent(getActivity(),PDFRenderFragment.class);
                        open_pdf_renderer.putExtra("file",result);
                        getActivity().startActivity(open_pdf_renderer);

//                        getFragmentManager().beginTransaction()
//                                .add(R.id.container, new PDFRenderFragment(),
//                                        "pdf_renderer_basic")
//                                .commit();
//
//                        FragmentManager fm = getActivity().getSupportFragmentManager();
//                        FragmentTransaction ft = fm.beginTransaction();
//                        ft.replace(R.id.fragment_layout, new PDFRenderFragment());
//                        ft.commit();
//                        mFileDescriptor = getActivity().getAssets().openFd(result).getParcelFileDescriptor();
//                        new PdfRenderer(mFileDescriptor);
                    }
                    catch(Exception e)
                    {

                    }

                }
                else
                {
                    Uri path = Uri.fromFile(pdfFile);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(path, "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(intent);
                }

            }
        }
    }


    @Override
    public void onPause()
    {
        super.onPause();
    }


    public void hello()
    {
        Log.v(" test "," hello ");
    }

    @Override
    public void onResume()
    {
//        if(pdf_frame_layout.getVisibility() == View.VISIBLE)
//        {
//
//        }
//        else
//        {
//            gridview.setVisibility(View.VISIBLE);
//        }
//        //hide the frame layout, and display the listview;
//        pdf_frame_layout.setVisibility(View.GONE);
//        list.setVisibility(View.VISIBLE);


        try{

            File file = new File(file_location_to_be_deleted);
            if(file.exists())
            {
                file.delete();
            }
            else
            {
                //file doesnt exist
            }

        }catch(Exception e){

            e.printStackTrace();

        }
        super.onResume();
    }

    public void refresh(String subject)
    {
        PostValue.set_current_paper_layout("papers");
        gridview.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {

        Log.v(" test "," on destroy called ");
        super.onDestroy();
    }

    //    @Override
//    public void onDestroyView() {
//
//        if(PostValue.get_current_paper_layout().equals("papers"))
//        {
//            super.onDestroyView();
//        }
//        else
//        {
//            PostValue.set_current_paper_layout("subjects");
//            gridview.setVisibility(View.VISIBLE);
//            list.setVisibility(View.GONE);
//            gridview.setAdapter(image_adapter);
//        }
//    }


}