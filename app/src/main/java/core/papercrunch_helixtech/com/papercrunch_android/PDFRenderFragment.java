package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by helixtech-android on 29/11/16.
 */
public class PDFRenderFragment extends Activity
{
    //SwipeGestureListener gestureListener;
    /**
     * Key string for saving the state of current page index.
     */
    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";

    /**
     * File descriptor of the PDF.
     */
    private ParcelFileDescriptor mFileDescriptor;

    /**
     * {@link android.graphics.pdf.PdfRenderer} to render the PDF.
     */
    private PdfRenderer mPdfRenderer;

    /**
     * Page that is currently shown on the screen.
     */
    private PdfRenderer.Page mCurrentPage;

    /**
     * {@link android.widget.ImageView} that shows a PDF page as a {@link android.graphics.Bitmap}
     */
    private ImageView mImageView;

    /**
     * {@link android.widget.Button} to move to the previous page.
     */
    private Button mButtonPrevious;

    /**
     * {@link android.widget.Button} to move to the next page.
     */
    private Button mButtonNext;

    public PDFRenderFragment() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pdf_render);

        mImageView = (ImageView) findViewById(R.id.image);

//        SwipeGestureListener gestureListener;
//        gestureListener = new SwipeGestureListener(getActivity());
//        mImageView.setOnTouchListener(gestureListener);
        mButtonPrevious = (Button) findViewById(R.id.previous);
        mButtonNext = (Button) findViewById(R.id.next);


        mButtonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 21)
                {
                    if((mCurrentPage.getIndex() - 1) == -1)
                    {
                        //do nothing
                    }
                    else
                    {
                        showPage(mCurrentPage.getIndex() - 1);
                    }

                }
            }
        });

        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 21) {
                    // Move to the next page
                    showPage(mCurrentPage.getIndex() + 1);
                }
            }
        });

//        try {
//            openRenderer(activity);
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(activity, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
//            activity.finish();
//        }

        try
        {
            if (Build.VERSION.SDK_INT >= 21) {

                Intent my_intent = getIntent();
                String file_loc = my_intent.getStringExtra("file");
                Log.v(" test "," string received is "+file_loc);
                File file = new File(file_loc);
                mPdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(file,ParcelFileDescriptor.MODE_READ_WRITE));
            }
        }
        catch(Exception e)
        {

        }



//        // Bind events.
//        mButtonPrevious.setOnClickListener(this);
//        mButtonNext.setOnClickListener(this);
        // Show the first page by default.
        int index = 0;
        // If there is a savedInstanceState (screen orientations, etc.), we restore the page index.
        if (null != savedInstanceState) {
            index = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 0);
        }
        showPage(index);
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        return inflater.inflate(R.layout.fragment_pdf_render, container, false);
//    }
//
//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        // Retain view references.
//        mImageView = (ImageView) view.findViewById(R.id.image);
//
////        SwipeGestureListener gestureListener;
////        gestureListener = new SwipeGestureListener(getActivity());
////        mImageView.setOnTouchListener(gestureListener);
//        mButtonPrevious = (Button) view.findViewById(R.id.previous);
//        mButtonNext = (Button) view.findViewById(R.id.next);
//
//
//        close_button = (Button) view.findViewById(R.id.close);
//
//        close_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((Papers_option_Fragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.container)).hello();
//            }
//        });
//
//        mButtonPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (Build.VERSION.SDK_INT >= 21)
//                {
//                    if((mCurrentPage.getIndex() - 1) == -1)
//                    {
//                        //do nothing
//                    }
//                    else
//                    {
//                        showPage(mCurrentPage.getIndex() - 1);
//                    }
//
//                }
//            }
//        });
//
//        mButtonNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (Build.VERSION.SDK_INT >= 21) {
//                    // Move to the next page
//                    showPage(mCurrentPage.getIndex() + 1);
//                }
//            }
//        });
////        // Bind events.
////        mButtonPrevious.setOnClickListener(this);
////        mButtonNext.setOnClickListener(this);
//        // Show the first page by default.
//        int index = 0;
//        // If there is a savedInstanceState (screen orientations, etc.), we restore the page index.
//        if (null != savedInstanceState) {
//            index = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 0);
//        }
//        showPage(index);
//    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            openRenderer(activity);
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(activity, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
//            activity.finish();
//        }
//    }

//    @Override
//    public void onDetach() {
//        try {
//            closeRenderer();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        super.onDetach();
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (Build.VERSION.SDK_INT >= 21) {
            if (null != mCurrentPage) {
                outState.putInt(STATE_CURRENT_PAGE_INDEX, mCurrentPage.getIndex());
            }
        }
    }

    /**
     * Sets up a {@link android.graphics.pdf.PdfRenderer} and related resources.
     */
    private void openRenderer(Context context) throws IOException {

        if (Build.VERSION.SDK_INT >= 21) {


            File file = new File(Environment.getExternalStorageDirectory()+"/"+"mypdf.pdf");
            //mPdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(file,ParcelFileDescriptor.MODE_READ_WRITE)‌​);

            mPdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(file,ParcelFileDescriptor.MODE_READ_WRITE));
        }
    }

    /**
     * Closes the {@link android.graphics.pdf.PdfRenderer} and related resources.
     *
     * @throws java.io.IOException When the PDF file cannot be closed.
     */
    private void closeRenderer() throws IOException {

        if (Build.VERSION.SDK_INT >= 21) {
            if (null != mCurrentPage) {
                mCurrentPage.close();
            }
            mPdfRenderer.close();
            mFileDescriptor.close();
        }
    }

    /**
     * Shows the specified page of PDF to the screen.
     *
     * @param index The page index.
     */
    private void showPage(int index) {

        if (Build.VERSION.SDK_INT >= 21)
        {
            if (mPdfRenderer.getPageCount() <= index) {
                return;
            }
            // Make sure to close the current page before opening another one.
            if (null != mCurrentPage) {
                mCurrentPage.close();
            }
            // Use `openPage` to open a specific page in PDF.
            mCurrentPage = mPdfRenderer.openPage(index);
            // Important: the destination bitmap must be ARGB (not RGB).
            Bitmap bitmap = Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(),
                    Bitmap.Config.ARGB_8888);
            // Here, we render the page onto the Bitmap.
            // To render a portion of the page, use the second and third parameter. Pass nulls to get
            // the default result.
            // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
            mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            // We are ready to show the Bitmap to user.
            mImageView.setImageBitmap(bitmap);
            updateUi();
        }
    }

    /**
     * Updates the state of 2 control buttons in response to the current page index.
     */
    private void updateUi() {

        if (Build.VERSION.SDK_INT >= 21) {
            int index = mCurrentPage.getIndex();
            int pageCount = mPdfRenderer.getPageCount();
//            mButtonPrevious.setEnabled(0 != index);
//            mButtonNext.setEnabled(index + 1 < pageCount);
            PDFRenderFragment.this.setTitle("hello");
        }
    }

    /**
     * Gets the number of pages in the PDF. This method is marked as public for testing.
     *
     * @return The number of pages.
     */
    public int getPageCount() {

        if (Build.VERSION.SDK_INT >= 21)
        {
            return mPdfRenderer.getPageCount();
        }
        return 0;
    }

    @Override
    public void onBackPressed() {
        finish();

        //now check if the app is in background or foreground.
        //If the app is in foreground, do nothing
        //Else if the app is in background, then open the main activity.

        if(isAppIsInBackground(PDFRenderFragment.this))
        {
            //open intent to display main activity
            Intent open_main_activity = new Intent(PDFRenderFragment.this,MainActivity.class);
            PDFRenderFragment.this.startActivity(open_main_activity);
        }
        else
        {
            //do nothing.
        }
        super.onBackPressed();
    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}