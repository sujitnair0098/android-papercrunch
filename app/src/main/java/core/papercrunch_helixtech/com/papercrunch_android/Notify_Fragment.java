package core.papercrunch_helixtech.com.papercrunch_android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by helixtech-android on 12/11/16.
 */
public class Notify_Fragment extends Fragment {

    public Notify_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.v(" test "," into notify fragment ");
        return inflater.inflate(R.layout.notify_fragment, container, false);
    }

}