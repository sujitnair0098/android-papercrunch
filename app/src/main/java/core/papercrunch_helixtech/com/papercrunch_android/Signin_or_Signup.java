package core.papercrunch_helixtech.com.papercrunch_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class Signin_or_Signup extends AppCompatActivity
{
    String encrypted_password;
    EditText username,password;
    Button signin;
    ProgressDialog progress;
    String f_name,l_name;
    String token = SharedPrefManager.getInstance(this).getDeviceToken();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_or_sign_up);
        username = (EditText) findViewById(R.id.username_edittext);
        password = (EditText) findViewById(R.id.password_edittext);
        signin = (Button) findViewById(R.id.done_button);

        signin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                progress = ProgressDialog.show(Signin_or_Signup.this, "Please Wait",
                        "", true);

                //encrypt the password before passing through URL.
                try {
                    encrypted_password = EncryptionClass.MD5(password.getText().toString());
                }
                catch(Exception e)
                {

                }
                String url = "http://httest.in//papercrunchws/RUC_UserLogin/" +
                        "emailId/"+username.getText().toString()+"/" +
                        "password/"+encrypted_password+"/" +
                        "deviceToken/"+token+"/"+
                        "deviceType/2";






                // http://httest.in//papercrunchws/RUC_UserLogin/emailId/prabha@helixtech.co/password/prabamca/deviceToken/test123secondtest/deviceType/1

//                ArrayList<String> user_id_list = DBOps.get_user_id(Signin_or_Signup.this,username.getText().toString());
//                PostValue.set_current_user_id(user_id_list.get(0));

                //As of now, making the log in procedure offline in order to test the edit profile
                //functionality and the edit academic details functionality.
                //This is done because the webservice for updating is not ready.

//                ArrayList<String> profile_details = DBOps.get_saved_profile_details(Signin_or_Signup.this,PostValue.get_current_user_id());
//                //the order of data is as follows.
//                //0-first name
//                //1-last name
//                //2-email id
//
//                if(profile_details.size() > 0)
//                {
//                    String result = profile_details.get(0);
//                    result = result.substring(result.indexOf("*")+1);
//                    result = result.substring(result.indexOf("*")+1);
//
//                    if(username.getText().toString().equals(result.substring(0,result.indexOf("*")))
//                            && encrypted_password.equals(result.substring(result.indexOf("*")+1)))
//                    {
//
//                        finish();
//                        Intent open_main_activity = new Intent(Signin_or_Signup.this,MainActivity.class);
//                        Signin_or_Signup.this.startActivity(open_main_activity);
//                    }
//                    else
//                    {
//                        progress.cancel();
//                        Toast.makeText(Signin_or_Signup.this,"Failed",Toast.LENGTH_LONG).show();
//                    }
//                }


                web_service_call(url);
            }
        });
    }


    private void web_service_call(String url)
    {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("Response", s);
                try {
                    Log.v(" test "," into try ");
                    JSONObject object = new JSONObject(s);
                    Log.v(" test "," response is "+object.getInt("response"));
                    if (object.getInt("response") == 1)
                    {
                        Log.v(" test "," sign up successful");
                        Toast.makeText(Signin_or_Signup.this,"Successful",Toast.LENGTH_SHORT).show();

                        f_name = object.getString("firstName");
                        l_name = object.getString("lastName");

                        //ArrayList<String> user_id_list = DBOps.get_user_id(Signin_or_Signup.this,username.getText().toString());
                        PostValue.set_current_user_id(object.getString("userid"));


                        //inserting the uid in database.
                        DBOps.insert_user_id(Signin_or_Signup.this,object.getString("userid"));

                        String MY_PREFS_NAME="mysharedpreferences";

                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("firstname",f_name);
                        editor.putString("lastname",l_name);
                        editor.putString("userid",object.getString("userid"));
                        editor.commit();

                        store_data();

                        finish();
                        Intent open_main_activity = new Intent(Signin_or_Signup.this,MainActivity.class);
                        Signin_or_Signup.this.startActivity(open_main_activity);

                    }
                    else
                    {
                        Toast.makeText(Signin_or_Signup.this,"Failed",Toast.LENGTH_SHORT).show();
                    }
                    progress.cancel();
                    //storeData(object, account);
                } catch (JSONException e)
                {
                    Log.v(" test "," into catch block ");
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)
            {
                Log.v(" test "," no internet connection ");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
//                data.put("designer_id", String.valueOf(SyncStateContract.Constants.designer_id));
//                return data;

                return null;

            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    public void store_data()
    {
        String MY_PREFS_NAME="mysharedpreferences";
        // MY_PREFS_NAME - a static String variable like:
//public static final String MY_PREFS_NAME = "MyPrefsFile";
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("username",username.getText().toString());
        editor.putString("password",encrypted_password);
        editor.commit();

        //also storing in local db.
        DBOps.insert_profile_details(Signin_or_Signup.this,PostValue.get_current_user_id(),f_name,l_name,username.getText().toString(),encrypted_password);
    }

    @Override
    public void onBackPressed()
    {
        Intent open_main_activity = new Intent(Signin_or_Signup.this,LoginActivity.class);
        Signin_or_Signup.this.startActivity(open_main_activity);
        super.onBackPressed();
    }
}
